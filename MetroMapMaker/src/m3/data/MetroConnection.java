package m3.data;

import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;

public class MetroConnection extends Line implements MetroElement{

    MetroStation s1;
    MetroStation s2;
    
    public MetroConnection(MetroStation stat1, MetroStation stat2){
        s1 = stat1;
        s2 = stat2;
        
        /*setStartX(s1.startX);
        setStartY(s1.startY);
        setEndX(s2.startX);
        setEndY(s2.startY);*/
        setStrokeWidth(6);
        
        this.startXProperty().bind(s1.centerXProperty());
        this.startYProperty().bind(s1.centerYProperty());
        this.endXProperty().bind(s2.centerXProperty());
        this.endYProperty().bind(s2.centerYProperty());
        
        this.setStrokeLineCap(StrokeLineCap.ROUND);
    }
    
    @Override
    public m3State getStartingState() {
        throw new UnsupportedOperationException("Connections do not have a starting state!");
    }

    @Override
    public void start(int x, int y) {
        throw new UnsupportedOperationException("Connections cannot be created alone!");
    }

    @Override
    public void drag(int x, int y) {
        throw new UnsupportedOperationException("Connections cannot be dragged!");
    }

    @Override
    public void size(int x, int y) {
        throw new UnsupportedOperationException("Connections cannot be sized like this!");
    }
    
    public void size(MetroStation s){
        /*if(s == s1){
            //System.out.println("s1");
            setStartX(s1.getCenterX());
            setStartY(s1.getCenterY());
        } else if (s == s2){
            //System.out.println("s2");
            setEndX(s2.getCenterX());
            setEndY(s2.getCenterY());
        }*/
    }

    @Override
    public void setLocation(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getWidth() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getHeight() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getX() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getY() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
