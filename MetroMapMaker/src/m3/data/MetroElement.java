package m3.data;

/**
 * This interface represents a family of metro map elements.
 * 
 * @author Stephen X Chen
 * @version 1.0
 */
public interface MetroElement {
    
    public m3State getStartingState();
    
    /**
     * Create the metro element at the specified x and y values
     * @param x X coordinate at which to create element
     * @param y Y coordinate at which to create element
     */
    public void start(int x, int y);

    /**
     * Drag the metro element (to move)
     * @param x Current X coordinate of cursor (while dragging)
     * @param y Current Y coordinate of cursor (while dragging)
     */
    public void drag(int x, int y);

    /**
     * Set the size of the metro element
     * @param x Width of element
     * @param y Height of element
     */
    public void size(int x, int y);

    /**
     * Directly set the location of the metro element
     * @param x X coordinate of desired location
     * @param y Y coordinate of desired location
     */
    public void setLocation(int x, int y);
    
    /**
     * Returns the width of the metro element
     * @return the width of the metro element
     */
    public double getWidth();

    /**
      Returns the height of the metro element
     * @return the height of the metro element
     */
    public double getHeight();

    /**
     * Returns the x coordinate of the metro element
     * @return the x coordinate of the metro element
     */
    public double getX();

    /**
     * Returns the y coordinate of the metro element
     * @return the y coordinate of the metro element
     */
    public double getY();
}
