package m3.data;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javax.imageio.ImageIO;

/**
 * This is a draggable image for our MetroMapMaker application.
 * 
 * @author Stephen Chen
 * @version 1.0
 */
public class MetroImage extends Rectangle implements MetroElement{

    int startX;
    int startY;
    Image image;
    String imgPath;
    
    public MetroImage(String path) {
	setX(0.0);
	setY(0.0);
	setOpacity(1.0);
        try {
            image = SwingFXUtils.toFXImage(ImageIO.read(new File(path)), null);
        } catch (IOException ex) {
        }
        setFill(new ImagePattern(image));
	startX = 0;
	startY = 0;
        imgPath = path;
        setWidth(image.getWidth());
        setHeight(image.getHeight());
        setStroke(Color.WHITE);
        setStrokeWidth(0);
    }
    
    @Override
    public void start(int x, int y){
	startX = x;
	startY = y;
    }

    @Override
    public void drag(int x, int y){
	double diffX = x - startX;
	double diffY = y - startY;
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	xProperty().set(newX);
	yProperty().set(newY);
	startX = x;
	startY = y;
    }

    @Override
    public void size(int x, int y){
    }

    @Override
    public void setLocation(int x, int y){
	xProperty().set(x);
	yProperty().set(y);
    }
    
    /**
     * Returns the path of the contained image
     * @return the path of the image
     */
    public String getPath() {
        return imgPath;
    }

    @Override
    public m3State getStartingState() {
	return m3State.STARTING_IMAGE;
    }
}
