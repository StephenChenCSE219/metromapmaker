package m3.data;

import java.util.ArrayList;

public class MetroPath {
    
    MetroStation startStation;
    MetroStation endStation;
    final int STATION_COST;
    final int TRANSFER_COST;
    public ArrayList<MetroLine> tripLines;
    public ArrayList<MetroStation> tripStations;
    public ArrayList<MetroStation> boardingStations;
    
    public MetroPath(MetroStation initStartStation, MetroStation initEndStation) {
        startStation = initStartStation;
        endStation = initEndStation;
        STATION_COST = 3;
        TRANSFER_COST = 10;
        tripLines = new ArrayList<>();
        tripStations = new ArrayList<>();
        boardingStations = new ArrayList<>();
    }
    /*
        clone

        This function clones this path object, returning a path initialized with all the same data.
    */
    public MetroPath duplicate() {
        MetroPath clonedPath = new MetroPath(this.startStation, this.endStation);
        for (int i = 0; i < this.tripLines.size(); i++)
            clonedPath.tripLines.add(this.tripLines.get(i));
        for (int i = 0; i < this.tripStations.size(); i++)
            clonedPath.tripStations.add(this.tripStations.get(i));
        for (int i = 0; i < this.boardingStations.size(); i++)
            clonedPath.boardingStations.add(this.boardingStations.get(i));
        return clonedPath;
    }

    /*
        addBoarding

        This function adds a boarding to the path. Note there is one boarding per line.
    */
    public void addBoarding(MetroLine boardingLine, MetroStation boardingStation) {
        tripLines.add(boardingLine);
        this.boardingStations.add(boardingStation);
    }

    /*
        getTripStations

        This function is for getting an array with a list of all the stations to be
        visited, including boarding and passing through while on a train.
    */
    public ArrayList<MetroStation> getTripStations() {
        tripStations = new ArrayList<>();

        if (isCompleteTrip()) {
            int i = 0;
            while (i < boardingStations.size() - 1) {
                ArrayList<MetroStation> stationsToAdd = generateStationsForPathOnLine(
                    this.tripLines.get(i), this.boardingStations.get(i), this.boardingStations.get(i+1));
                for (int j = 0; j < stationsToAdd.size(); j++) {
                    MetroStation stationToAdd = stationsToAdd.get(j);
                    if (!this.tripStations.contains(stationToAdd)) {
                        this.tripStations.add(stationToAdd);
                    }
                }
                i++;
            }
            ArrayList<MetroStation> stationsToAdd = this.generateStationsForPathOnLine(
                    this.tripLines.get(i), this.boardingStations.get(i), this.endStation);
            for (int k = 0; k < stationsToAdd.size(); k++) {
                MetroStation stationToAdd = stationsToAdd.get(k);
                this.tripStations.add(stationToAdd);
            }
        }
        return this.tripStations;
    }

    /*
        isCompleteTrip

        This function tests to see if this path is complete, meaning one can get from its
        start station toe its end station using the boarding stops and trip lines.
    */
    public boolean isCompleteTrip() {
        if (tripLines.isEmpty()) {
            return false;
        }

        if (!this.tripLines.get(this.tripLines.size() - 1).stations.contains(endStation)) {
            return false;
        }

        // NOW, ARE ALL THE BOARDING STATIONS ON ALL THE TRIP LINES, IF NOT IT'S INCORRECT
        for (int i = 0; i < this.boardingStations.size(); i++) {
            if (!this.tripLines.get(i).stations.contains(boardingStations.get(i))) {
                return false;
            }
        }

        // IF WE MADE IT THIS FAR WE KNOW IT'S A COMPLETE TRIP'
        return true;
    }

    /*
        generateStationsForPathOnLine

        This function returns a list of all the stations to be visited to get from one station
        to another on the same line.
    */
    public ArrayList<MetroStation> generateStationsForPathOnLine(MetroLine line, MetroStation station1, MetroStation station2) {
        ArrayList<MetroStation> stationsOnPath = new ArrayList<>();
        int station1Index = line.stations.indexOf(station1);
        int station2Index = line.stations.indexOf(station2);
        
        // FOR CIRCULAR LINES WE CAN GO IN EITHER DIRECTION
        /*if (line.circular) {
            if (station1Index >= station2Index) {
                var forward = station1Index - station2Index;
                var reverse = station2Index + line.stations.length - station1Index;
                if (forward < reverse) {
                    for (var i = station1Index; i >= station2Index; i--) {
                        var stationToAdd = line.stations[i];
                        stationsOnPath.push(stationToAdd);
                    }
                }
                else {
                    for (var i = station1Index; i < line.stations.length; i++) {
                        var stationToAdd = line.stations[i];
                        stationsOnPath.push(stationToAdd);
                    }
                    for (var i = 0; i <= station2Index; i++) {
                        var stationToAdd = line.stations[i];
                        stationsOnPath.push(stationToAdd);
                    }
                }
            }
            // STILL CIRCULAR, BUT station1 IS BEFORE station2 IN THE ARRAY
            else {
                var forward = station2Index - station1Index;
                var reverse = station1Index + line.stations.length - station2Index;
                if (forward < reverse) {
                    for (var i = station1Index; i <= station2Index; i++) {
                        var stationToAdd = line.stations[i];
                        stationsOnPath.push(stationToAdd);
                    }
                }
                else {
                    for (var i = station1Index; i >= 0; i--) {
                        var stationToAdd = line.stations[i];
                        stationsOnPath.push(stationToAdd);
                    }
                    for (var i = line.stations.length - 1; i >= station2Index; i--) {
                        var stationToAdd = line.stations[i];
                        stationsOnPath.push(stationToAdd);
                    }
                }
            }
        }
        // NOT CIRCULAR
        else {*/
        if (station1Index >= station2Index) {
            for (int i = station1Index; i >= station2Index; i--) {
                MetroStation stationToAdd = line.stations.get(i);
                stationsOnPath.add(stationToAdd);
            }
        }
        else {
            for (int i = station1Index; i <= station2Index; i++) {
                MetroStation stationToAdd = line.stations.get(i);
                stationsOnPath.add(stationToAdd);
            }
        }
        //}
        return stationsOnPath;
    }


    /*
        hasLine

        This function tests to see if this trip includes the testLineName. If it does, true
        is returned, otherwise false.
    */
    public boolean hasLine(MetroLine test) {
        return tripLines.contains(test);
    }

    /*
        hasLineWithStation

        This function tests to see if this trip has a line with the testStationName 
        station on it. If so, true is returned, false otherwise.
    */
    public boolean hasLineWithStation(MetroStation test) {
        // GO THROUGH ALL THE LINES AND SEE IF IT'S IN ANY OF THEM'
        for (int i = 0; i < this.tripLines.size(); i++) {
            if (tripLines.get(i).stations.contains(test))
                return true;
        }
        return false;
    }

    /*
        calculateTimeOfTrip

        This function calculates and returns the time of this trip taking into account
        the time it takes for a train to go from station to station is constant as is
        the time it takes to transfer lines.
    */
    public int calculateTimeOfTrip() {
        ArrayList<MetroStation> stations = getTripStations();
        int stationsCost = (stations.size() - 1) * this.STATION_COST;
        int transferCost = (this.tripLines.size() - 1) * this.TRANSFER_COST;
        return stationsCost + transferCost;
    }
}
