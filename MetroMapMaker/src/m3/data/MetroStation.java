package m3.data;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Objects;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 * This is a metro station for our MetroMapMaker application.
 * 
 * @author Stephen Chen
 * @version 1.0
 */
public class MetroStation extends Circle implements MetroElement{

    int startX;
    int startY;
    public ArrayList<MetroConnection> connections;
    boolean isEnd;
    
    public MetroLabel label;
    public int labelPos;
    
    public LinkedList<MetroStation> path;
    public boolean isVisited;
    
    String id;
    
    public MetroStation(){
        setCenterX(0.0);
	setCenterY(0.0);
	setRadius(7.0);
	setOpacity(1.0);
        startX = 0;
        startY = 0;
        
        label = new MetroLabel("");
        label.hasParent = true;
        labelPos = 0;
        isVisited = false;
        path = new LinkedList<>();
        
        label.translateXProperty().bind(this.centerXProperty().add(this.radiusProperty()));
        label.translateYProperty().bind(this.centerYProperty().subtract(this.radiusProperty()));
        
        connections = new ArrayList<>();
    }
    
    @Override
    public m3State getStartingState() {
	return m3State.STARTING_STATION;
    }
    
    public void moveLabel(int dir){
        label.translateXProperty().unbind();
        label.translateYProperty().unbind();
        
        labelPos += dir;
        if(labelPos < 0)
            labelPos = 3;
        if(labelPos > 3)
            labelPos = 0;
        
        switch(labelPos){
            case 0:
                label.translateXProperty().bind(this.centerXProperty().add(this.radiusProperty()));
                label.translateYProperty().bind(this.centerYProperty().subtract(this.radiusProperty()));
                break;
            case 1:
                label.translateXProperty().bind(this.centerXProperty().add(this.radiusProperty()));
                label.translateYProperty().bind((this.centerYProperty().add(this.radiusProperty())).add(label.getHeight()));
                break;
            case 3:
                label.translateXProperty().bind(this.centerXProperty().subtract(this.radiusProperty()).subtract(label.getWidth()));
                label.translateYProperty().bind(this.centerYProperty().subtract(this.radiusProperty()));
                break;
            case 2:
                label.translateXProperty().bind(this.centerXProperty().subtract(this.radiusProperty()).subtract(label.getWidth()));
                label.translateYProperty().bind((this.centerYProperty().add(this.radiusProperty())).add(label.getHeight()));
                break;
        }
    }
    
    public void setLabelPos(int pos){
        labelPos = pos-1;
        moveLabel(1);
    }
    
    @Override
    public void start(int x, int y) {
        startX = x;
        startY = y;
        
        //label.setX(x + labelOffsetX);
        //label.setY(y + labelOffsetY);
    }

    @Override
    public void drag(int x, int y) {
        double diffX = x - startX;
	double diffY = y - startY;
	double newX = getCenterX() + diffX;
	double newY = getCenterY() + diffY;
	setCenterX(newX);
	setCenterY(newY);
	startX = x;
	startY = y;
        
        /*if((connection.getStartX() != connection.getEndX()) || (connection.getStartY() != connection.getEndY())){
            if(!isEnd) {
                connection.setStartX(startX);
                connection.setStartY(startY);
            } else {
                connection.setEndX(startX);
                connection.setEndY(startY);
            }
        }*/
        /*
        for(MetroConnection connection:connections){
            if(this == connection.s1){
                connection.setStartX(startX);
                connection.setStartY(startY);
            }else if(this == connection.s2){
                connection.setEndX(startX);
                connection.setEndY(startY);
            }
        }*/
        
        //label.setX(x + labelOffsetX);
        //label.setY(y + labelOffsetY);
    }

    @Override
    public void size(int x, int y) {
	double width = x - startX;
	double height = y - startY;
	double centerX = startX + (width / 2);
	double centerY = startY + (height / 2);
	setCenterX(centerX);
	setCenterY(centerY);
	//setRadius(width / 2);
	//setRadius(height / 2);	
    }

    @Override
    public void setLocation(int x, int y) {
        setCenterX(x + getRadius());
        setCenterY(y + getRadius());
    }

    @Override
    public double getWidth() {
	return getRadius() * 2;
    }

    @Override
    public double getHeight() {
	return getRadius() * 2;
    }

    @Override
    public double getX() {
	return getCenterX() - getRadius();
    }

    @Override
    public double getY() {
	return getCenterY() - getRadius();
    }
    
    @Override
    public String toString(){
        return label.getText();
    }
    
    public void setName(String s){
        label.setText(s);
    }
    
    public void setID(String s){
        id = s;
    }
    
    public String getID(){
        return id;
    }
    
    public boolean isEnd(){
        return isEnd;
    }

    public void rotateLabel(int i) {
        if(i > 0)
            label.setRotate(label.getRotate() + 90);
        else
            label.setRotate(label.getRotate() - 90);
    }
}
