package m3.data;

/**
 * This enum has the various possible states of the metro map maker app
 * during the editing process which helps us determine which controls
 * are usable or not and what specific user actions should affect.
 * 
 * @author Stephen X Chen
 * @version 1.0
 */
public enum m3State {
    SELECTING_ELEMENT,
    DRAGGING_ELEMENT,
    STARTING_LINE,
    STARTING_STATION,
    STARTING_IMAGE,
    STARTING_LABEL,
    SIZING_ELEMENT,
    SELECTING_STATION_TO_ADD,
    SELECTING_STATION_TO_REMOVE,
    DRAGGING_NOTHING,
    SIZING_NOTHING,
}
