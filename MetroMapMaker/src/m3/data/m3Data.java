package m3.data;

import m3.tps.ChangeMapSize_Transaction;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.scene.paint.Color;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import static m3.data.m3State.SELECTING_ELEMENT;
import static m3.data.m3State.SIZING_ELEMENT;
import static m3.data.m3State.STARTING_IMAGE;
import static m3.data.m3State.STARTING_LABEL;
import static m3.data.m3State.STARTING_LINE;
import static m3.data.m3State.STARTING_STATION;
import m3.gui.m3Workspace;
import m3.tps.AddImage_Transaction;
import m3.tps.AddLabel_Transaction;
import m3.tps.AddLine_Transaction;
import m3.tps.AddStationToLine_Transaction;
import m3.tps.AddStation_Transaction;
import m3.tps.ChangeBackground_Transaction;
import m3.tps.ChangeColor_Transaction;
import m3.tps.ChangeFont_Transaction;
import m3.tps.ChangeLineThickness_Transaction;
import m3.tps.ChangeMapSize_Transaction;
import m3.tps.ChangeStationProperties_Transaction;
import m3.tps.ChangeStationRadius_Transaction;
import m3.tps.MoveElement_Transaction;
import m3.tps.MoveLabel_Transaction;
import m3.tps.RemoveElement_Transaction;
import m3.tps.RotateLabel_Transaction;

/**
 * This class serves as the data management component for this application.
 *
 * @author Stephen X Chen
 * @version 1.0
 */
public class m3Data implements AppDataComponent{

    ObservableList<Node> elements;
    Shape newElement;
    MetroLine newLine;
    Shape selectedElement;
    MetroLine selectedLine;
    String selectedFont;
    int selectedSize;
    static jTPS jTPS = new jTPS();
    m3State state;
    AppTemplate app;
    Effect highlightedEffect;
    
    ArrayList<MetroStation> metroStations;
    ArrayList<MetroLine> metroLines;
    
    int mapZoom;
    int mapPositionX;
    int mapPositionY;
    int clickedButNotDraggedX;
    int clickedButNotDraggedY;
    double startLineThickness;
    double endLineThickness;
    double startCircleRadius;
    double endCircleRadius;
    
    WritableImage mapImage;
    
    public String mapName;
    
    /**
     * This constructor creates the data manager and sets up the 
     * @param initApp The application within which this data manager is serving.
     */
    public m3Data(AppTemplate initApp){
        // KEEP THE APP FOR LATER
	app = initApp;

        mapName = "";
        
	// NO SHAPE STARTS OUT AS SELECTED
	newElement = null;
	selectedElement = null;
        metroStations = new ArrayList<>();
        metroLines = new ArrayList<>();
        selectedFont = "Arial";
        selectedSize = 12;
        
        state = m3State.SELECTING_ELEMENT;
	
	// THIS IS FOR THE SELECTED SHAPE
	DropShadow dropShadowEffect = new DropShadow();
	dropShadowEffect.setOffsetX(0.0f);
	dropShadowEffect.setOffsetY(0.0f);
	dropShadowEffect.setSpread(1.0);
	dropShadowEffect.setColor(Color.YELLOW);
	dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
	dropShadowEffect.setRadius(8);
	highlightedEffect = dropShadowEffect;
    }
    
    /**
     * Resets all data for a new map
     */
    @Override
    public void resetData() {
	setState(SELECTING_ELEMENT);
	newElement = null;
	selectedElement = null;

	// INIT THE COLORS
	//currentFillColor = Color.web(WHITE_HEX);
	//currentOutlineColor = Color.web(BLACK_HEX);
	
	elements.clear();
	((m3Workspace)app.getWorkspaceComponent()).getCanvas().getChildren().clear();
    }
    
    public void setFont(String f){
        selectedFont = f;
        if(selectedElement instanceof MetroLabel){
            jTPS_Transaction transaction = new ChangeFont_Transaction((MetroLabel)selectedElement, selectedFont, selectedSize, false, false);
            jTPS.addTransaction(transaction);
        }
        if(selectedElement instanceof MetroStation){
            jTPS_Transaction transaction = new ChangeFont_Transaction(((MetroStation)selectedElement).label, selectedFont, selectedSize, false, false);
            jTPS.addTransaction(transaction);
        }
    }
    
    public void setFontSize(int i){
        selectedSize = i;
        if(selectedElement instanceof MetroLabel){
            jTPS_Transaction transaction = new ChangeFont_Transaction((MetroLabel)selectedElement, selectedFont, selectedSize, false, false);
            jTPS.addTransaction(transaction);
        }
        if(selectedElement instanceof MetroStation){
            jTPS_Transaction transaction = new ChangeFont_Transaction(((MetroStation)selectedElement).label, selectedFont, selectedSize, false, false);
            jTPS.addTransaction(transaction);
        }
        
    }
    
    /**
     * Start creating a new element
     */
    public void initNewElement(){
        if(isInState(STARTING_STATION))
            initNewStation();
        if(isInState(STARTING_LINE))
            initNewLine();
        if(isInState(STARTING_IMAGE))
            initNewImage();
        if(isInState(STARTING_LABEL))
            initNewLabel();
    }
    
    /**
     * Select the topmost element at the given location
     * @param x x coordinate
     * @param y y coordinate
     * @return
     */
    public Shape selectTopElement(int x, int y){
        m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
        Shape shape = getTopElement(x, y);
        
	if (selectedElement != null) {
	    unhighlightShape(selectedElement);
	}
        if(shape instanceof MetroStation){
	//if (shape != null) {
            if (!(((MetroStation)shape).isEnd)) {
                ((MetroElement)shape).start(x, y);
                highlightShape(shape);
            }
            else {
                workspace.stationsComboBox.getSelectionModel().select(null);
                workspace.stationFill.setValue((Color)shape.getFill());
            }
	    //workspace.loadSelectedShapeSettings(shape);
	} else if (shape instanceof MetroLabel /*&& !(((MetroLabel)shape).hasParent)*/){
            //TODO: load correct settings into the combo boxes
            ((MetroElement)shape).start(x, y);
            highlightShape(shape);
        } else if (shape instanceof MetroImage){
            ((MetroElement)shape).start(x, y);
            highlightShape(shape);
        }
	selectedElement = shape;
        
	return shape;
    }
    
    /**
     * Get the topmost element at the given location
     * @param x x coordinate
     * @param y y coordinate
     * @return
     */
    public Shape getTopElement(int x, int y){
	for (int i = elements.size() - 1; i >= 0; i--) {
	    Shape shape = (Shape)elements.get(i);
	    if (shape.contains(x, y)) {
		return shape;
	    }
	}
	return null;
    }
    
    /**
     * Adds the given element to the map
     * @param elementToAdd element to add to the map
     */
    public void addElement(Shape elementToAdd){
	elements.add(elementToAdd);
    }
    
    /**
     * Removes the given element from the map
     * @param elementToRemove element to be removed from the map
     */
    public void removeElement(MetroElement elementToRemove){
	elements.remove(elementToRemove);
    }
    
    /**
     * Removes the selected element
     */
    public void removeSelectedElement(){
	if(selectedElement instanceof MetroImage || selectedElement instanceof MetroLabel){
            jTPS_Transaction transaction = new RemoveElement_Transaction(selectedElement, this);
            jTPS.addTransaction(transaction);
        }
    }
    
    /**
     * Moves the selected element to the back
     */
    public void moveSelectedElementToBack(){
    }
    
    /**
     * Moves the selected element to the front
     */
    public void moveSelectedElementToFront(){
    }
    
    public void moveStationsToFront(){
        ObservableList<Node> stationsList = FXCollections.observableArrayList();
        ObservableList<Node> newList = FXCollections.observableArrayList();
        
        try{
            for (Node node : elements) {
                if(node instanceof MetroStation || node instanceof MetroLabel || node instanceof MetroImage)
                    stationsList.add(node);
                else
                    newList.add(node);
            }
        
            for (Node node : stationsList)
                newList.add(node);
            
            //System.out.println(elements);
            //System.out.println(newList);
            
            elements.clear();
            
            for (Node node : newList)
                elements.add(node);
            
        } catch(IllegalArgumentException e){}
    }
    
    /**
     * Check if the given state is the same as the current state
     * @param testState state to check against current state
     * @return true if test state = current state, otherwise false
     */
    public boolean isInState(m3State testState){
	return state == testState;
    }
    
    /**
     * Move the element to exact location
     * @param element element to move
     * @param x x coordinate
     * @param y y coordinate
     */
    public void movedElement(MetroElement element, int endX, int endY){
            if (!(clickedButNotDraggedX == endX && clickedButNotDraggedY == endY)){
            
            element.drag(endX, endY);
            
            jTPS_Transaction transaction = new MoveElement_Transaction(element, clickedButNotDraggedX, clickedButNotDraggedY, element.getX(), element.getY());
            jTPS.addTransaction(transaction);
        }
    }
    
    public void snapStation(){
        MetroStation s = (MetroStation)selectedElement;
        s.setLocation((int)(Math.round((s.startX)/30)*30 + s.getRadius()), (int)(Math.round((s.startY)/30)*30 + s.getRadius()));
    }
    
    /**
     * Toggles label text bold
     */
    public void toggleBoldText(){
        if(selectedElement instanceof MetroLabel){
            jTPS_Transaction transaction = new ChangeFont_Transaction((MetroLabel)selectedElement, selectedFont, selectedSize, true, false);
            jTPS.addTransaction(transaction);
        }
        if(selectedElement instanceof MetroStation){
            jTPS_Transaction transaction = new ChangeFont_Transaction(((MetroStation)selectedElement).label, selectedFont, selectedSize, true, false);
            jTPS.addTransaction(transaction);
        }
    }
    
    /**
     * Toggles label text italics
     */
    public void toggleItalicizeText(){
        if(selectedElement instanceof MetroLabel){
            jTPS_Transaction transaction = new ChangeFont_Transaction((MetroLabel)selectedElement, selectedFont, selectedSize, false, true);
            jTPS.addTransaction(transaction);
        }
        if(selectedElement instanceof MetroStation){
            jTPS_Transaction transaction = new ChangeFont_Transaction(((MetroStation)selectedElement).label, selectedFont, selectedSize, false, true);
            jTPS.addTransaction(transaction);
        }
    }
    
    /**
     * Set the fill color of the current element
     * @param isStation is the element a station?
     * @param initColor color to fill current element
     */
    public void setCurrentElementFillColor(boolean isStation, Color initColor){
        if (selectedElement != null){
            if((selectedElement instanceof MetroStation && isStation) || (selectedElement instanceof MetroLabel && !isStation)){
                jTPS_Transaction transaction = new ChangeColor_Transaction(selectedElement, initColor);
                jTPS.addTransaction(transaction);
            }
            if(selectedElement instanceof MetroStation && !isStation){
                jTPS_Transaction transaction = new ChangeColor_Transaction(((MetroStation)selectedElement).label, initColor);
                jTPS.addTransaction(transaction);
            }
        }
    }
    
    /**
     * Set the border width of the current element
     * @param initBorderWidth size of border for current element
     */
    public void setCurrentElementThickness(int initBorderWidth){
    }

    public void setClickedButNotDraggedPos(int x, int y) {
        clickedButNotDraggedX = x;
        clickedButNotDraggedY = y;
    }

    public void setState(m3State initState) {
	state = initState;
    }

    public void initNewLine(){
	// DESELECT THE SELECTED SHAPE IF THERE IS ONE
	if (selectedElement != null) {
	    unhighlightShape(selectedElement);
	    selectedElement = null;
	}
        
        elements.add(newLine.getBegin());
        elements.add(newLine.getConn());
        elements.add(newLine.getEnd());
        
        //newElement = newLine.getBegin().getConnection();
        newElement = newLine.endPoint;
        highlightShape(newElement);
	
	// GO INTO SHAPE SIZING MODE
	state = SIZING_ELEMENT;
    }
    
    public void startNewLine(int x, int y) {
	MetroLine line = new MetroLine();
	line.start(x, y);
        
        line.getBegin().setCenterX(x);
        line.getEnd().setCenterX(x);
        line.getBegin().setCenterY(y);
        line.getEnd().setCenterY(y);
        
	newLine = line;
	initNewLine();
    }

    public void initNewStation() {
        // DESELECT THE SELECTED SHAPE IF THERE IS ONE
	if (selectedElement != null) {
	    unhighlightShape(selectedElement);
	    selectedElement = null;
	}
        
        //ObservableList<String> dispStations = FXCollections.observableArrayList();
        //dispStations = ((m3Workspace)app.getWorkspaceComponent()).displayStations;
        int dupe = 1;
        String toAdd = newElement.toString();
        for(MetroStation m : metroStations){
            if(m.id.equals(toAdd)){
            toAdd = newElement.toString() + dupe;
            dupe++;
            }
        }
        //System.out.println(((MetroStation)newElement).id);
        ((MetroStation)newElement).setID(toAdd);
        //dispStations.add(toAdd);
        metroStations.add((MetroStation)newElement);
        
        jTPS_Transaction transaction = new AddStation_Transaction((MetroStation)newElement, this);
        jTPS.addTransaction(transaction);
        
        ((m3Workspace)app.getWorkspaceComponent()).addStationToComboBox((MetroStation)newElement);
        
        //((m3Workspace)app.getWorkspaceComponent()).stationsComboBox.getSelectionModel().select(metroStations.indexOf(newElement));
        
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.DEFAULT);
        state = SELECTING_ELEMENT;
    }
    
    public void startNewStation(int x, int y) {
	MetroStation newEllipse = new MetroStation();
        
        m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
        TextInputDialog ti = new TextInputDialog();
        ti.setHeaderText("");
        ti.setTitle("Name of Station");
        ti.setGraphic(null);
        Optional<String> text = ti.showAndWait();
        
        if (text.isPresent()){
            newEllipse.setName(text.get());
        }
        
	newEllipse.start(x, y);
        newEllipse.setCenterX(x);
        newEllipse.setCenterY(y);
        newEllipse.setFill(workspace.stationFill.getValue());
        newEllipse.setStroke(Color.BLACK);
        newEllipse.setStrokeWidth(3);
        newEllipse.setRadius(workspace.stationCircleRadius.getValue());
	newElement = newEllipse;
	initNewStation();
    }

    public void startNewImage(int x, int y) {
        FileChooser fc = new FileChooser();
        
        ArrayList<String> imageExtensions = new ArrayList<>();
        imageExtensions.add("*.JPG");
        imageExtensions.add("*.JPEG");
        imageExtensions.add("*.PNG");
        imageExtensions.add("*.GIF");
        imageExtensions.add("*.BMP");
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Image Files", imageExtensions);
        fc.getExtensionFilters().add(filter);
 
        File file = fc.showOpenDialog(null);
        
        if (file != null) {
            MetroImage newIMG = new MetroImage(file.getPath());
            newIMG.start(x, y);
            newIMG.setX(x);
            newIMG.setY(y);
            newElement = newIMG;
            initNewImage();
        }
    }

    public void initNewImage(){
	if (selectedElement != null) {
	    unhighlightShape(selectedElement);
	    selectedElement = null;
	}
	
	// ADD THE SHAPE TO THE CANVAS
	jTPS_Transaction transaction = new AddImage_Transaction((MetroImage)newElement, this);
        jTPS.addTransaction(transaction);
        highlightShape(newElement);        
        
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.DEFAULT);
        state = SELECTING_ELEMENT;
    }
    
    public void startNewLabel(int x, int y) {
        m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
        TextInputDialog ti = new TextInputDialog();
        ti.setHeaderText("");
        ti.setTitle("Text to Add");
        ti.setGraphic(null);
        Optional<String> text = ti.showAndWait();
        
        if (text.isPresent()){
            MetroLabel newTXT = new MetroLabel(text.get());
            //newTXT.setFont(new Font(workspace.getFontComboBox().getValue().toString(), (int)workspace.getSizeComboBox().getValue()));
            newTXT.start(x, y);
            newTXT.setX(x);
            newTXT.setY(y);
            newElement = newTXT;
            initNewLabel();
        }
        
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.DEFAULT);
        state = SELECTING_ELEMENT;
    }
    
    public void initNewLabel(){
        if (selectedElement != null) {
	    unhighlightShape(selectedElement);
	    selectedElement = null;
	}
	
	// ADD THE SHAPE TO THE CANVAS
	jTPS_Transaction transaction = new AddLabel_Transaction((MetroLabel)newElement, this);
        jTPS.addTransaction(transaction);
        highlightShape(newElement);
    }
    
    public Shape getNewElement(){
        return newElement;
    }
    
    public MetroLine getNewLine(){
        return newLine;
    }
    
    public Shape getSelectedElement(){
        return selectedElement;
    }
    
    public void selectSizedElement(){
	if (selectedElement != null)
	    unhighlightShape(selectedElement);
        
        selectedLine = newLine;
        editSelectedLine();
        
        newLine.getEnd().start((int)((MetroStation)newElement).getX(), (int)(((MetroStation)newElement)).getY()); //why the hell does this work
        
        //ObservableList<String> dispLines = FXCollections.observableArrayList();
        //dispLines = ((m3Workspace)app.getWorkspaceComponent()).displayLines;
        int dupe = 1;
        String toAdd = newLine.toString();
        for(MetroLine l : metroLines){
            if(l.id.equals(toAdd)){
                toAdd = newLine.toString() + dupe;
                dupe++;
            }
        }
        System.out.println(newLine.id);
        //dispLines.add(toAdd);
        newLine.setID(toAdd);
        metroLines.add(newLine);
        
        ((m3Workspace)app.getWorkspaceComponent()).addLineToComboBox(newLine);
        
	selectedElement = newElement;
	highlightShape(selectedElement);
	newElement = null;
        
        moveStationsToFront();
        
        elements.remove(newLine.getBegin());
        elements.remove(newLine.getConn());
        elements.remove(newLine.getEnd());
        
	jTPS_Transaction transaction = new AddLine_Transaction(newLine, this);
        jTPS.addTransaction(transaction);
        
	if (state == SIZING_ELEMENT) {
	    //state = ((MetroElement)selectedElement).getStartingState();
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            state = SELECTING_ELEMENT;
	}
    }

    private void highlightShape(Shape element) {
	element.setEffect(highlightedEffect);
    }

    private void unhighlightShape(Shape selectedElement) {
	selectedElement.setEffect(null);
    }

    public ObservableList<Node> getElements() {
	return elements;
    }
    
    public void setElements(ObservableList<Node> initShapes) {
	elements = initShapes;
    }
    
    public void setSelectedElement(Shape initSelectedShape) {
	selectedElement = initSelectedShape;
    }

    public void selectLine(int i){
        if(i >= 0){
            selectedLine = metroLines.get(i);
            m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
            workspace.lineColor.setBackground(new Background(new BackgroundFill(selectedLine.conn.getStroke(), new CornerRadii(15), null)));
            workspace.lineColor.setText(((Color)(workspace.lineColor.getBackground().getFills().get(0).getFill())).toString());
            workspace.lineColor.setTextFill(((Color)(workspace.lineColor.getBackground().getFills().get(0).getFill())).invert());
        }
    }
    
    public void selectLine(MetroLine line){
        if(line == null)
            return;
        
        selectedLine = line;
        m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
        workspace.lineColor.setBackground(new Background(new BackgroundFill(selectedLine.conn.getStroke(), new CornerRadii(15), null)));
        workspace.lineColor.setText(((Color)(workspace.lineColor.getBackground().getFills().get(0).getFill())).toString());
        workspace.lineColor.setTextFill(((Color)(workspace.lineColor.getBackground().getFills().get(0).getFill())).invert());

    }
    
    public void selectStation(int i){
        if(i < metroStations.size())
            return;
        Shape shape = metroStations.get(i);
        if (selectedElement != null) {
	    unhighlightShape(selectedElement);
	}
	if (shape != null) {
	    highlightShape(shape);
	    m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
            workspace.stationsComboBox.getSelectionModel().select((MetroStation)shape);
            workspace.stationFill.setValue((Color)shape.getFill());
	    //workspace.loadSelectedShapeSettings(shape);
	}
	selectedElement = shape;
    }
    
    public void selectStationToAdd(int x, int y) {
        MetroStation toAdd = null;
        for (int i = elements.size() - 1; i >= 0; i--) {
	    Shape shape = (Shape)elements.get(i);
	    if (shape.contains(x, y) && (shape instanceof MetroStation)) {
		toAdd = (MetroStation)elements.get(i);
                addToLine(toAdd);
	    }
	}
        
        selectStation(metroStations.indexOf(toAdd));
        
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.DEFAULT);
        state = SELECTING_ELEMENT;
    }
    
    private void addToLine(MetroStation toAdd){
        //System.out.println();
        if(selectedLine.stations.contains(toAdd))
            return;
        
        ObservableList<Node> oldList = FXCollections.observableArrayList();
        for (Node node : elements)
            oldList.add(node);
        
        ArrayList<MetroConnection> connectionsToAdd = selectedLine.addStation(toAdd);
        elements.add(connectionsToAdd.get(0));
        elements.add(connectionsToAdd.get(1));
        
        moveStationsToFront();
        cleanUpLines();
        
        jTPS_Transaction transaction = new AddStationToLine_Transaction(oldList, elements, this);
        jTPS.addTransaction(transaction);
        //System.out.println(connectionsToAdd);
        //connectionsToAdd.remove(0);
        //if(connectionsToAdd.size() > 1)
        //    connectionsToAdd.remove(1);
        //MetroConnection toRemove1 = connectionsToAdd.get(2);
        //MetroConnection toRemove2 = connectionsToAdd.get(3);
        
        //if(toRemove1 != null)
        //    elements.remove(toRemove1);
        //if(toRemove2 != null)
        //    elements.remove(toRemove2);
        
        //if(connectionsToAdd.size() >= 3)
        //    connectionsToAdd.remove(2);
        //if(connectionsToAdd.size() >= 4)
        //    connectionsToAdd.remove(3);
        //System.out.println(connectionsToAdd);
        
        //System.out.println(connectionsToAdd);
        //for(Shape s : connectionsToAdd){
        //    highlightShape(s);}
        
    }
    
    private void removeFromLine(MetroStation toRemove){
        
        if(selectedLine.startPoint.equals(toRemove) || selectedLine.endPoint.equals(toRemove))
            return;
        
        MetroConnection connectionToAdd = selectedLine.removeStation(toRemove);
        elements.add(connectionToAdd);
        
        moveStationsToFront();
        cleanUpLines();
    }
    
    private void cleanUpLines(){
        ArrayList<Node> toDel = new ArrayList<>();
        for(Node n : elements){
            if ((n instanceof MetroConnection) && ((MetroConnection)n).getOpacity() == 0.2)
                toDel.add(n);
        }
        for(Node n : toDel){
            elements.remove(n);
        }
        
                
    }

    public void editSelectedLine() {
        if(selectedLine == null)
            return;
        SplitPane editLinePane = new SplitPane();
        ColorPicker lineColor = new ColorPicker((Color)selectedLine.conn.getStroke());
        TextField txt = new TextField(selectedLine.startPoint.label.getText());
        editLinePane.getItems().add(lineColor);
        editLinePane.getItems().add(txt);
        
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("");
        alert.setGraphic(null);
        alert.setTitle("Select Line Color and Name");
        alert.getDialogPane().setContent(editLinePane);
        alert.showAndWait();
        
        if(txt.getText() != null)
            selectedLine.setName(txt.getText());
        
        selectedLine.setColor(lineColor.getValue());
        
        //ObservableList<String> dispLines = ((m3Workspace)app.getWorkspaceComponent()).displayLines;
        int dupe = 1;
        String toAdd = selectedLine.toString();
        for(MetroLine l : metroLines){
            if(l.id.equals(toAdd)){
                toAdd = selectedLine.toString() + dupe;
                dupe++;
            }
        }
        //System.out.println(selectedLine.id);
        selectedLine.setID(toAdd);
        m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
        workspace.lineColor.setBackground(new Background(new BackgroundFill(selectedLine.conn.getStroke(), new CornerRadii(15), null)));
        workspace.lineColor.setText(((Color)(workspace.lineColor.getBackground().getFills().get(0).getFill())).toString());
        workspace.lineColor.setTextFill(((Color)(workspace.lineColor.getBackground().getFills().get(0).getFill())).invert());
        
        //TODO: FIX THIS SO THE CORRECT LINE NAME APPEARS IN THE SELECTION BOX (SMALL ISSUE)
        //((m3Workspace)app.getWorkspaceComponent()).linesComboBox.getSelectionModel().select(selectedLine);
        /*
        TextInputDialog ti = new TextInputDialog();
        ti.setHeaderText("");
        ti.setTitle("Name of Line");
        ti.setGraphic(null);
        ColorPicker lineColor = new ColorPicker(Color.BLACK);
        ti.getDialogPane().getChildren().add(lineColor);
        Optional<String> text = ti.showAndWait();
        
        if (text.isPresent()){
            selectedLine.setName(text.get());
        }*/
    }

    public void updateDisplayLine(String s) {
        m3Workspace wrk = (m3Workspace)app.getWorkspaceComponent();
        //wrk.displayLines.set(wrk.displayLines.indexOf(s), selectedLine.id);

        //System.out.println(s + " : " + wrk.displayLines.indexOf(s));
        //int index = wrk.displayLines.indexOf(s);
        //wrk.displayLines.remove(index);
        //if(index == wrk.displayLines.size())
        //    wrk.displayLines.add(selectedLine.id);
        //else
        //    wrk.displayLines.add(index, selectedLine.id);
        //selectLine(index);
        //wrk.linesComboBox.getSelectionModel().select(index);
    }

    public void selectStationToRemove(int x, int y) {
        MetroStation toRemove = null;
        for (int i = elements.size() - 1; i >= 0; i--) {
	    Shape shape = (Shape)elements.get(i);
	    if (shape.contains(x, y) && (shape instanceof MetroStation)) {
		toRemove = (MetroStation)elements.get(i);
                removeFromLine(toRemove);
	    }
	}
        
        selectStation(metroStations.indexOf(toRemove));
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.DEFAULT);
        state = SELECTING_ELEMENT;
    }

    public void removeStation() {
        if(selectedElement == null)
            return;
        
        if(!(selectedElement instanceof MetroStation))
            return;
                    
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Delete Line");
        alert.setHeaderText("Are you sure you want to delete " + selectedElement + "?");
        ButtonType yesButton = new ButtonType("Yes");
        ButtonType noButton = new ButtonType("No");

        alert.getButtonTypes().setAll(yesButton, noButton);
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == noButton)
            return;
        
        MetroStation toRemove = (MetroStation)selectedElement;
        MetroLine prevSel = selectedLine;
        for(MetroLine line : metroLines){
            selectLine(metroLines.indexOf(line));
            if(line.stations.contains(toRemove))
                removeFromLine(toRemove);
        }
        selectedLine = prevSel;
        //workspace.displayStations.remove(toRemove.id);
        removeElement(toRemove.label);
        removeElement(toRemove);
        removeStationFromComboBox(toRemove);
    }

    public void removeSelectedLine() {
        if(selectedLine == null)
            return;
        
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Delete Line");
        alert.setHeaderText("Are you sure you want to delete " + selectedLine + "?");
        ButtonType yesButton = new ButtonType("Yes");
        ButtonType noButton = new ButtonType("No");

        alert.getButtonTypes().setAll(yesButton, noButton);
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == noButton)
            return;
      
        while(selectedLine.stations.size() > 2){
            removeFromLine(selectedLine.stations.get(1));
        }
        
        for(MetroConnection c : selectedLine.startPoint.connections)
            c.setOpacity(0.2);
        cleanUpLines();
        
        m3Workspace wrk = (m3Workspace) app.getWorkspaceComponent();
        //wrk.displayLines.remove(metroLines.indexOf(selectedLine));
        
        elements.remove(selectedLine.startPoint.label);
        elements.remove(selectedLine.startPoint);
        elements.remove(selectedLine.endPoint.label);
        elements.remove(selectedLine.endPoint);
        elements.remove(selectedLine.conn);
        
        metroLines.remove(selectedLine);
        //System.out.println(elements);
        
        removeLineFromComboBox(selectedLine);
        
        selectedLine = null;
    }

    public String getMapName() {
        return mapName;
    }

    public ArrayList<MetroLine> getLines() {
        return metroLines;
    }

    public ArrayList<MetroStation> getStations() {
        return metroStations;
    }
    
    public void setExportImage(WritableImage image){
        mapImage = image;
    }

    public Image getMapImage() {
        return mapImage;
    }
    
    public void loadMapName(){
        Scanner in;
        try {
            in = new Scanner(new FileReader("work/temp"));
            StringBuilder sb = new StringBuilder();
            while(in.hasNext()) {
                sb.append(in.next());
            }
            in.close();
            mapName = sb.toString();
        } catch (FileNotFoundException ex) {
            mapName = "Error in loading map name";
        }
    }

    public void addStation(MetroStation s) {
        metroStations.add(s);
        elements.add(s);
        elements.add(s.label);
    }

    public void addLine(MetroLine line) {
        metroLines.add(line);
        elements.add(line.getBegin());
        elements.add(line.getConn());
        elements.add(line.getEnd());
        elements.add(line.getBegin().label);
        elements.add(line.getEnd().label);
    }
    
    public void undo(){
        jTPS.undoTransaction();
    }
    
    public void redo(){
        jTPS.doTransaction();
    }

    public void listCurrentLineStations() {
        if (selectedLine == null)
            return;
        
        String stationsInLine = "";

        for(int i = 1; i < selectedLine.getStations().size()-1; i++)
            stationsInLine += selectedLine.getStations().get(i).label.getText() + "\n";
        
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setGraphic(null);
        alert.setTitle("List Stations");
        alert.setHeaderText(selectedLine.getBegin().label.getText() + " Line Stops");
        alert.setContentText(stationsInLine);
        alert.showAndWait();
    }

    public void setCurrentLineThickness(double value) {
        if(selectedLine != null)
            selectedLine.setThickness(value);
    }

    public void setLineThicknessStartPos(double value) {
        startLineThickness = value;
    }

    public void setLineThicknessEndPos(double value) {
        endLineThickness = value;
    }

    public void setLineThickness(double value) {
        jTPS_Transaction transaction = new ChangeLineThickness_Transaction(selectedLine, ((m3Workspace)app.getWorkspaceComponent()).lineThickness, startLineThickness, endLineThickness);
        jTPS.addTransaction(transaction);
    }

    public void setCircleRadiusEndPos(double value) {
        endCircleRadius = value;
    }

    public void setCircleRadius(double value) {
        if(selectedElement instanceof MetroStation){
            jTPS_Transaction transaction = new ChangeStationRadius_Transaction((MetroStation)selectedElement, ((m3Workspace)app.getWorkspaceComponent()).stationCircleRadius, startCircleRadius, endCircleRadius);
            jTPS.addTransaction(transaction);
        }
    }

    public void setCircleRadiusStartPos(double value) {
        startCircleRadius = value;
    }

    public void setCurrentCircleRadius(double value) {
        if(selectedElement != null && selectedElement instanceof MetroStation)
            ((MetroStation)selectedElement).setRadius(value);
    }
    
    public void findRoute(MetroStation s1, MetroStation s2){
        /*MetroPath route = findMinimumTransferPath(s1, s2);
        System.out.println(route.tripStations);
        System.out.println(route.boardingStations);
        System.out.println(route.tripLines);*/
        
        if(s1.equals(s2)){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Route from " + s1 + " to " + s2);
            alert.setHeaderText("You are already at " + s1 + "!");
            alert.showAndWait();
        } else {
            bfs(s1);
            LinkedList<MetroStation> route = s2.path;
            if(route.isEmpty()){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Route from " + s1 + " to " + s2);
                alert.setHeaderText("No route found!");
                alert.showAndWait();
            } else {
                int totalStops = 0;
                ArrayList<MetroLine> visitedLines = new ArrayList<>();
                ArrayList<Integer> visitedLineLength = new ArrayList<>();
                for(int i = 0; i < route.size()-1; i++){
                    for(MetroLine l : metroLines){
                        if(l.stations.contains(route.get(i)) && l.stations.contains(route.get(i+1))){
                            visitedLines.add(l);
                            visitedLineLength.add(l.stations.indexOf(route.get(i+1)) - l.stations.indexOf(route.get(i)));
                            totalStops += l.stations.indexOf(route.get(i+1)) - l.stations.indexOf(route.get(i));
                        }
                    }
                }
                String directionsString = "Board " + visitedLines.get(0) + " at " + s1;

                String linesString = "";
                for(int i = 0; i < visitedLines.size(); i++){
                    linesString += "\n" + visitedLines.get(i) + " (" + visitedLineLength.get(i) + " stops)";
                    if(i!=0)
                        directionsString += "\nTransfer to " + visitedLines.get(i) + " at " + route.get(i);
                }
                directionsString += "\nDisembark " + visitedLines.get(visitedLines.size()-1) + " at " + s2;

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Route from " + s1 + " to " + s2);
                alert.setHeaderText(
                        "Origin: " + s1 +
                        "\nDestination: " + s2 +
                        linesString +
                        "\nTotal Stops: " + totalStops +
                        "\nEstimated Time: " +(totalStops * 3 + visitedLines.size() * 10) + " minutes");
                alert.setContentText(directionsString);
                alert.showAndWait();
            }
        } 
    }
    
    @SuppressWarnings("unchecked")
    public void bfs(MetroStation station){
        for(Node s: elements){
            if(s instanceof MetroStation){
                ((MetroStation)s).isVisited = false;
                ((MetroStation)s).path.clear();
            }
        }
        
        Queue<MetroStation> queue = new LinkedList<MetroStation>();
        
        station.path.add(station);
        queue.add(station);
        station.isVisited = true;
        
        while(!queue.isEmpty()){
            MetroStation m = queue.poll();
            
            ArrayList<MetroLine> containingLines = new ArrayList<>();
            ArrayList<MetroStation> connectedStations = new ArrayList<>();
            /*for(MetroConnection conn : station.connections){
                System.out.println(conn.s1 + " and " + conn.s2);
                if(conn.s1.equals(station) && !conn.s2.isEnd)
                    connectedStations.add(conn.s2);
                if(conn.s2.equals(station) && !conn.s1.isEnd)
                    connectedStations.add(conn.s1);
            }*/
            for(MetroLine l : metroLines){
                if(l.stations.contains(m))
                    containingLines.add(l);
            }
            
            for(MetroLine l : containingLines){
                for(MetroStation connected : l.stations)
                    connectedStations.add(connected);
            }
            
            
            
            for(MetroStation s : connectedStations){
                if (!s.isVisited){
                    s.isVisited = true;
                    LinkedList<MetroStation> npath = (LinkedList<MetroStation>)m.path.clone();
                    npath.add(s);
                    s.path = npath;
                    queue.add(s);
                }
            }
        }
        
    }
    
    public MetroPath findMinimumTransferPath(MetroStation start, MetroStation end){
        //MetroStation start = startStation;
        //MetroStation end = endStation;
        ArrayList<MetroLine> linesToTest = new ArrayList<>();
        ArrayList<MetroLine> visitedLines = new ArrayList<>();

        // THIS WILL COUNT HOW MANY TRANSFERS
        int numTransfers = 0;

        // THESE WILL BE PATHS THAT WE WILL BUILD TO TEST
        ArrayList<MetroPath> testPaths = new ArrayList<>();

        // START BY PUTTING ALL THE LINES IN THE START STATION
        // IN OUR linesToTest Array
        //for (var i = 0; i < startStation.lines.length; i++) {
        //    var path = new M3Path(startStation, endStation);
        //    testPaths.push(path);
        //    path.addBoarding(startStation.lines[i], startStation);
        //}
        
        ArrayList<MetroLine> startLines = new ArrayList<>();
        for(MetroLine line : metroLines){
            for(MetroConnection conn : start.connections) {
                MetroStation test = conn.s1;
                if(conn.s1.equals(start))
                    test = conn.s2;
                if(line.stations.contains(test) && !startLines.contains(line))
                    startLines.add(line);
            }
        }
        
        System.out.println(startLines);
        
        for (int i = 0; i < startLines.size(); i++) {           
            MetroPath path = new MetroPath(start, end);
            testPaths.add(path);
            path.addBoarding(startLines.get(i), start);
        }

        boolean found = false;
        boolean morePathsPossible = true;
        ArrayList<MetroPath> completedPaths = new ArrayList<>();
        while (!found && morePathsPossible) {
            ArrayList<MetroPath> updatedPaths = new ArrayList<>();
            for (int i = 0; i < testPaths.size(); i++) {
                MetroPath testPath = testPaths.get(i);

                // FIRST CHECK TO SEE IF THE DESTINATION IS ALREADY ON THE PATH
                if (testPath.hasLineWithStation(end)) {
                    completedPaths.add(testPath);
                    found = true;
                    morePathsPossible = false;
                }
                else if (morePathsPossible) {
                    // GET ALL THE LINES CONNECTED TO THE LAST LINE ON THE TEST PATH
                    // THAT HAS NOT YET BEEN VISITED
                    MetroLine lastLine = testPath.tripLines.get(testPath.tripLines.size() - 1);
                    
                    ArrayList<MetroLine> lastLineTransferNames = new ArrayList<>();
                    for (MetroStation m : lastLine.stations){
                        for (MetroLine line : metroLines){
                            if(line.stations.contains(m) && !line.equals(lastLine))
                                lastLineTransferNames.add(line);
                        }
                    }
                    
                    for (int j = 0; j < lastLineTransferNames.size(); j++) {
                        //var testLineName = lastLine.transferNames[j];
                        MetroLine testLine = lastLineTransferNames.get(j);
                        if (!testPath.hasLine(testLine)) {
                            MetroPath newPath = testPath.duplicate();
                            //MetroStation intersectingStation = lastLine.findIntersectingStation(testLine);
                            
                            MetroStation intersectingStation = new MetroStation();
                            //lastLine find intersection station with testLines
                            for(MetroStation m : lastLine.stations){
                                if(testLine.stations.contains(m))
                                    intersectingStation = m;
                            }
                            
                            System.out.println(intersectingStation);
                            
                            newPath.addBoarding(testLine, intersectingStation);
                            updatedPaths.add(newPath);
                        }
                        // DEAD ENDS DON'T MAKE IT TO THE NEXT ROUND
                    }
                }
            }
            if (updatedPaths.size() > 0) {
                testPaths = updatedPaths;
                numTransfers++;
            }
            else {
                morePathsPossible = false;
            }
        }
        // WAS A PATH FOUND?
        if (found) {
            MetroPath shortestPath = completedPaths.get(0);
            int shortestTime = shortestPath.calculateTimeOfTrip();
            for (int i = 1; i < completedPaths.size(); i++) {
                MetroPath testPath = completedPaths.get(i);
                int timeOfTrip = testPath.calculateTimeOfTrip();
                if (timeOfTrip < shortestTime) {
                    shortestPath = testPath;
                    shortestTime = timeOfTrip;
                }
            }
            // WE NOW KNOW THE SHORTEST PATH, COMPLETE ITS DATA FOR EASY USE
            return shortestPath;
        }
        // NO PATH FOUND
        else {
            return null;
        }
    }

    public void addAllToComboBox() {
        for(MetroLine line : metroLines)
            ((m3Workspace)app.getWorkspaceComponent()).addLineToComboBox(line);
        for(MetroStation station : metroStations)
            ((m3Workspace)app.getWorkspaceComponent()).addStationToComboBox(station);
    }

    public void moveSelectedStationLabel() {
        if(selectedElement instanceof MetroStation){
            jTPS_Transaction transaction = new MoveLabel_Transaction((MetroStation)selectedElement);
            jTPS.addTransaction(transaction);
        }
    }

    public void rotateSelectedStationLabel() {
        if(selectedElement instanceof MetroStation){
            jTPS_Transaction transaction = new RotateLabel_Transaction((MetroStation)selectedElement);
            jTPS.addTransaction(transaction);
        }
    }

    public void setBackground(Background bg, Pane p) {
        jTPS_Transaction transaction = new ChangeBackground_Transaction(p, bg);
        jTPS.addTransaction(transaction);
    }
    
    public void setBackground(String path, Pane p) {
        jTPS_Transaction transaction = new ChangeBackground_Transaction(p, path);
        jTPS.addTransaction(transaction);
    }

    public void changeMapSize(double d) {
        m3Workspace wrk = (m3Workspace)(app.getWorkspaceComponent());
        jTPS_Transaction transaction = new ChangeMapSize_Transaction(d, wrk);
        jTPS.addTransaction(transaction);
    }
    
    public Pane getBackground(){
        m3Workspace wrk = (m3Workspace)(app.getWorkspaceComponent());
        return wrk.getBackground();
    }

    private void removeLineFromComboBox(MetroLine selectedLine) {
        ((m3Workspace)app.getWorkspaceComponent()).removeLineFromComboBox(selectedLine);
    }

    private void removeStationFromComboBox(MetroStation toRemove) {
        ((m3Workspace)app.getWorkspaceComponent()).removeStationFromComboBox(toRemove);
    }
}
