package m3.data;

import com.sun.javafx.tk.FontLoader;
import com.sun.javafx.tk.Toolkit;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * This is a draggable label for our MetroMapMaker application.
 * 
 * @author Stephen X Chen
 * @version 1.0
 */
public class MetroLabel extends Text implements MetroElement{
    
    int startX;
    int startY;
    public boolean isBold;
    public boolean isItalics;
    public boolean hasParent;
    
    public MetroLabel(String text) {
        super(text);
	setX(0.0);
	setY(0.0);
	setOpacity(1.0);
	startX = 0;
	startY = 0;
        
        isBold = false;
        isItalics = false;
        hasParent = false;
        
        setStroke(Color.WHITE);
        setStrokeWidth(0);
    }
    
    @Override
    public void start(int x, int y) {
	startX = x;
	startY = y;
    }
    
    @Override
    public void drag(int x, int y) {
	double diffX = x - startX;
	double diffY = y - startY;
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	xProperty().set(newX);
	yProperty().set(newY);
	startX = x;
	startY = y;
    }
    
    @Override
    public void size(int x, int y) {
    }
    
    @Override
    public void setLocation(int x, int y) {
	xProperty().set(x);
	yProperty().set(y);
    }

    /**
     * Deprecated
     * @return -1
     */
    @Override
    public double getWidth() {
        FontLoader fontLoader = Toolkit.getToolkit().getFontLoader();
        return fontLoader.computeStringWidth(this.getText(), this.getFont());
    }

    /**
     * Deprecated
     * @return -1
     */
    @Override
    public double getHeight() {
        return this.getFont().getSize();
    }
    
    /**
     * Toggles label text bold
     */
    public void toggleBold() {
        isBold = !isBold;
        Font currentFont = this.getFont();
        this.setFont(Font.font(currentFont.getFamily(), getFontWeight(), getFontPosture(), currentFont.getSize()));
    }
    
    /**
     * Toggles label text italics
     */
    public void toggleItalics() {
        isItalics = !isItalics;
        Font currentFont = this.getFont();
        this.setFont(Font.font(currentFont.getFamily(), getFontWeight(), getFontPosture(), currentFont.getSize()));
    }
    
    public FontWeight getFontWeight(){
        if (isBold)
            return FontWeight.BOLD;
        return FontWeight.NORMAL;
    }
    
    public FontPosture getFontPosture(){
        if (isItalics)
            return FontPosture.ITALIC;
        return FontPosture.REGULAR;
    }
    
    /**
     * Attaches this label to the given element
     * @param e element to attach this label to
     */
    public void setParent(MetroElement e){
        
    }

    @Override
    public m3State getStartingState() {
	return m3State.STARTING_LABEL;
    }
}
