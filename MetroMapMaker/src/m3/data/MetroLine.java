package m3.data;

import java.util.ArrayList;
import java.util.Objects;
import javafx.scene.paint.Color;

/**
 * This is a metro line for our MetroMapMaker application.
 * 
 * @author Stephen Chen
 * @version 1.0
 */
public class MetroLine implements MetroElement{

    int beginX;
    int beginY;
    
    MetroStation startPoint;
    MetroStation endPoint;
    MetroConnection conn;
    
    public String id;
    
    ArrayList<MetroStation> stations;
    
    public MetroLine(){
        startPoint = new MetroStation();
        startPoint.setFill(Color.YELLOW);
        startPoint.setRadius(10);
        startPoint.setOpacity(0);
        startPoint.isEnd = true;
        startPoint.id = startPoint + "START";
        endPoint = new MetroStation();
        endPoint.isEnd = true;
        endPoint.setFill(Color.YELLOW);
        endPoint.setRadius(10);
        endPoint.setOpacity(0);
        endPoint.id = endPoint.id + "END";
        endPoint.label.hasParent = true;
        startPoint.label.hasParent = true;
        
        endPoint.label.setOnMouseEntered(e->{
	    endPoint.setOpacity(255);
	});
        endPoint.label.setOnMouseExited(e->{
	    endPoint.setOpacity(0);
	});
        endPoint.setOnMouseEntered(e->{
	    endPoint.setOpacity(255);
	});
        endPoint.setOnMouseExited(e->{
	    endPoint.setOpacity(0);
	});
        
        startPoint.label.setOnMouseEntered(e->{
	    startPoint.setOpacity(255);
	});
        startPoint.label.setOnMouseExited(e->{
	    startPoint.setOpacity(0);
	});
        startPoint.setOnMouseEntered(e->{
	    startPoint.setOpacity(255);
	});
        startPoint.setOnMouseExited(e->{
	    startPoint.setOpacity(0);
	});
        
        id = "";
        stations = new ArrayList<>();
        stations.add(startPoint);
        stations.add(endPoint);
    }
    
    @Override
    public void start(int x, int y) {
        startPoint.start(x, y);
        //startPoint.connection.setStartX(startPoint.startX);
        //startPoint.connection.setStartY(startPoint.startY);
        //startPoint.connection.setEndX(startPoint.startX);
        //startPoint.connection.setEndY(startPoint.startY);
        endPoint.start(x, y);
        //endPoint.connection = startPoint.connection;
        conn = new MetroConnection(startPoint, endPoint);
        startPoint.connections.add(conn);
        endPoint.connections.add(conn);
    }

    @Override
    public void drag(int x, int y) {
    }

    @Override
    public void size(int x, int y) {
        endPoint.setLocation(x,y);
        conn.size(endPoint);
        //startPoint.getConnection().setEndX(endPoint.getCenterX());
        //startPoint.getConnection().setEndY(endPoint.getCenterY());
    }

    @Override
    public void setLocation(int x, int y) {
    }

    @Override
    public double getWidth() {
        return 0;
    }

    @Override
    public double getHeight() {
        return 0;
    }

    @Override
    public double getX() {
        return 0;
    }

    @Override
    public double getY() {
        return 0;
    }

    public MetroStation getBegin(){
        return startPoint;
    }
    
    public MetroStation getEnd(){
        return endPoint;
    }
    
    public MetroConnection getConn(){
        return conn;
    }
    
    @Override
    public m3State getStartingState() {
	return m3State.STARTING_LINE;
    }
    
    @Override
    public String toString(){
        return startPoint.label.getText();
        //return id;
    }
    
    public void setName(String s){
        startPoint.label.setText(s);
        endPoint.label.setText(s);
        
        startPoint.id = s + "START";
        endPoint.id = s + "END";
    }
    
    public void setID(String s){
        id = s;
    }

    public ArrayList<MetroConnection> addStation(MetroStation toAdd) {
        MetroStation prev;
        MetroStation next;
        int insert = 0;
        
        //System.out.println(stations);
        
        if(stations.size() == 2){
            //stations.add(toAdd);
            prev = startPoint;
            next = endPoint;
        } else {
            MetroStation closest;
            double distance;
            
            closest = startPoint;
            distance = Math.sqrt(Math.pow(startPoint.getCenterX() - toAdd.getCenterX(), 2) + Math.pow(startPoint.getCenterY() - toAdd.getCenterY(), 2));
            insert = 0;
            
            for(int i = 0; i < stations.size(); i++){
                MetroStation s = stations.get(i);
                if (Math.sqrt(Math.pow(s.getCenterX() - toAdd.getCenterX(), 2) + Math.pow(s.getCenterY() - toAdd.getCenterY(), 2)) < distance) {
                    distance = Math.sqrt(Math.pow(s.getCenterX() - toAdd.getCenterX(), 2) + Math.pow(s.getCenterY() - toAdd.getCenterY(), 2));
                    closest = s;
                    insert = i;
                }
            }
            /*
            if (Math.sqrt(Math.pow(endPoint.getCenterX() - toAdd.getCenterX(), 2) + Math.pow(endPoint.getCenterY() - toAdd.getCenterY(), 2)) < distance) {
                    distance = Math.sqrt(Math.pow(endPoint.getCenterX() - toAdd.getCenterX(), 2) + Math.pow(endPoint.getCenterY() - toAdd.getCenterY(), 2));
                    closest = endPoint;
                    insert = stations.size()-1;
            }*/
         
            //System.out.println(insert);
            
            /*if(insert == 0)
                prev = startPoint;
            else
                prev = stations.get(insert);
            
            if(insert + 1 >= stations.size())
                next = endPoint;
            else
                next = stations.get(insert + 1);*/
            prev = stations.get(insert);
            if(insert + 1 >= stations.size()){
                next = endPoint;
                prev = stations.get(insert-1);
            }
            else{
                next = stations.get(insert + 1);
            }
        }
        
        if(insert+1 < stations.size())
            stations.add(insert+1, toAdd);
        else
            stations.add(stations.size()-1, toAdd);
        
        //System.out.println("prev: " + prev);
        //System.out.println("next: " + next);
        
        //now we have the stations to insert the new station between. delete the connection between the two stations and link them up to the new station.
        
        int deleteCase1 = -1;
        int deleteCase2 = -1;
        
        for(int i = 0; i < prev.connections.size(); i++){
            MetroConnection c = prev.connections.get(i);
            if((c.s1 == prev && c.s2 == next) || (c.s2 == prev && c.s1 == next)){
                deleteCase1 = i;
            }
        }
        for(int i = 0; i < next.connections.size(); i++){
            MetroConnection c = next.connections.get(i);
            if((c.s1 == prev && c.s2 == next) || (c.s2 == prev && c.s1 == next)){
                deleteCase2 = i;
            }
        }
        
        //if(deleteCase1 > -1)
        //    removedConnection1 = prev.connections.get(deleteCase1);
        //if(deleteCase2 > -1)
        //    removedConnection2 = next.connections.get(deleteCase2);
        MetroConnection removedConnection1 = null;
        MetroConnection removedConnection2 = null;
        if(deleteCase1 > -1){
            removedConnection1 = prev.connections.get(deleteCase1);
            prev.connections.get(deleteCase1).setOpacity(0.2);
            prev.connections.remove(deleteCase1);}
        if(deleteCase2 > -1){
            removedConnection2 = next.connections.get(deleteCase2);
            next.connections.get(deleteCase2).setOpacity(0.2);
            next.connections.remove(deleteCase2);}
        
        MetroConnection prevToNew = new MetroConnection(prev, toAdd);
        prevToNew.setStroke(conn.getStroke());
        MetroConnection newToNext = new MetroConnection(toAdd, next);
        newToNext.setStroke(conn.getStroke());
        
        prev.connections.add(prevToNew);
        next.connections.add(newToNext);
        toAdd.connections.add(prevToNew);
        toAdd.connections.add(newToNext);
        
        ArrayList<MetroConnection> addedConnections = new ArrayList<>();
        addedConnections.add(prevToNew);
        addedConnections.add(newToNext);
        //if(removedConnection1 != null)
        //    addedConnections.add(removedConnection1);
        //if(removedConnection2 != null && removedConnection2 != removedConnection1)
        //    addedConnections.add(removedConnection2);
        
        return addedConnections;
    }
    
    public MetroConnection removeStation(MetroStation toRemove) {
        int index = stations.indexOf(toRemove);
        MetroStation prev = null;
        MetroStation next = null;
        if(index-1 >= 0)
            prev = stations.get(index-1);
        else
            prev = startPoint;
        
        if(index+1 >= 0)
            next = stations.get(index+1);
        else
            next = endPoint;
        MetroConnection con = new MetroConnection(prev, next);
        
        int deleteCase1 = -1;
        int deleteCase2 = -1;
        MetroConnection removedConnection1 = null;
        MetroConnection removedConnection2 = null;
        
        for(int i = 0; i < prev.connections.size(); i++){
            MetroConnection c = prev.connections.get(i);
            if((c.s1 == prev && c.s2 == toRemove) || (c.s1 == toRemove && c.s2 == prev)){
                deleteCase1 = i;
            }
        }
        
        for(int i = 0; i < next.connections.size(); i++){
            MetroConnection c = next.connections.get(i);
            if((c.s1 == next && c.s2 == toRemove) || (c.s1 == toRemove && c.s2 == next)){
                deleteCase2 = i;
            }
        }
        
        if(deleteCase1 > -1){
            removedConnection1 = prev.connections.get(deleteCase1);
            prev.connections.get(deleteCase1).setOpacity(0.2);
            prev.connections.remove(removedConnection1);
            toRemove.connections.remove(removedConnection1);
        }
        if(deleteCase2 > -1){
            removedConnection2 = next.connections.get(deleteCase2);
            next.connections.get(deleteCase2).setOpacity(0.2);
            next.connections.remove(removedConnection2);
            toRemove.connections.remove(removedConnection2);
        }
        
        con.setStroke(conn.getStroke());
        prev.connections.add(con);
        next.connections.add(con);
        
        //removedConnection1 = prev.connections.get(deleteCase1);
        //removedConnection2 = next.connections.get(deleteCase2);
        
        toRemove.connections.remove(removedConnection1);
        toRemove.connections.remove(removedConnection2);
        
        stations.remove(toRemove);
        
        return con;
    }

    public void setColor(Color c) {
        conn.setStroke(c);
        for(MetroStation s : stations){
            for(MetroConnection con : s.connections) {
                if(stations.contains(con.s1) && stations.contains(con.s2))
                    con.setStroke(c);
            }
        }
    }

    public ArrayList<MetroStation> getStations() {
        return stations;
    }

    public void setStations(ArrayList<MetroStation> newS) {
        stations.clear();
        stations = newS;
    }   
    
    public void setConn(MetroConnection c){
        conn = c;
    }

    public void setThickness(double value) {
        conn.setStrokeWidth(value);
        for(MetroStation s : stations){
            for(MetroConnection con : s.connections) {
                if(stations.contains(con.s1) && stations.contains(con.s2))
                    con.setStrokeWidth(value);
            }
        }
    }
}
