package m3.gui;

import djf.AppTemplate;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.shape.Shape;
import m3.data.MetroElement;
import m3.data.MetroImage;
import m3.data.MetroLabel;
import m3.data.MetroStation;
import m3.data.m3Data;
import m3.data.m3State;
import static m3.data.m3State.*;

/**
 * This class responds to interactions with the rendering surface.
 *
 * @author Stephen X Chen
 * @version 1.0
 */
public class CanvasController {
    
    AppTemplate app;
    
    public CanvasController(AppTemplate initApp) {
        app = initApp;
    }
    
    /**
     * Respond to mouse presses on the rendering surface, which we call canvas,
     * but is actually a ScrollPane.
     * @param x mouse x coordinate
     * @param y mouse x coordinate
     */
    public void processCanvasMousePress(int x, int y) {
        m3Data dataManager = (m3Data) app.getDataComponent();
        if (dataManager.isInState(SELECTING_ELEMENT)) {
            // SELECT THE TOP SHAPE
            Shape element = dataManager.selectTopElement(x, y);
            Scene scene = app.getGUI().getPrimaryScene();

            // AND START DRAGGING IT
            if (element instanceof MetroStation || element instanceof MetroImage || (element instanceof MetroLabel /*&& !(((MetroLabel)element).hasParent)*/)) {
                scene.setCursor(Cursor.MOVE);
                dataManager.setClickedButNotDraggedPos(((int)((MetroElement)element).getX()), ((int)((MetroElement)element).getY()));
                dataManager.setState(m3State.DRAGGING_ELEMENT);
                app.getGUI().updateToolbarControls(false);
            } else {
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
        } else if (dataManager.isInState(m3State.SELECTING_STATION_TO_ADD)){
            dataManager.selectStationToAdd(x, y);
        } else if (dataManager.isInState(m3State.SELECTING_STATION_TO_REMOVE)){
            dataManager.selectStationToRemove(x, y);
        } else if (dataManager.isInState(m3State.STARTING_LINE)) {
            dataManager.startNewLine(x, y);
        } else if (dataManager.isInState(m3State.STARTING_STATION)) {
            dataManager.startNewStation(x, y);
        } else if (dataManager.isInState(m3State.STARTING_IMAGE)) {
            dataManager.startNewImage(x, y);
        } else if (dataManager.isInState(m3State.STARTING_LABEL)) {
            dataManager.startNewLabel(x, y);
        }
        m3Workspace workspace = (m3Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a ScrollPane.
     * @param x mouse x coordinate
     * @param y mouse x coordinate
     */
    public void processCanvasMouseDragged(int x, int y) {
        m3Data dataManager = (m3Data) app.getDataComponent();
        if (dataManager.isInState(SIZING_ELEMENT)) {
            MetroElement newDraggableShape = dataManager.getNewLine();
            newDraggableShape.size(x, y);
        } else if (dataManager.isInState(DRAGGING_ELEMENT)) {
            MetroElement selectedDraggableShape = (MetroElement) dataManager.getSelectedElement();
            selectedDraggableShape.drag(x, y);
            app.getGUI().updateToolbarControls(false);
        }
    }

    /**
     * Respond to mouse button release on the rendering surface, which we call canvas,
     * but is actually a ScrollPane.
     * @param x mouse x coordinate
     * @param y mouse x coordinate
     */
    public void processCanvasMouseRelease(int x, int y) {
        m3Data dataManager = (m3Data) app.getDataComponent();
        if (dataManager.isInState(SIZING_ELEMENT)) {
            dataManager.selectSizedElement();
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(m3State.DRAGGING_ELEMENT)) {
            dataManager.setState(SELECTING_ELEMENT);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            
            MetroElement selectedShape = (MetroElement) dataManager.getSelectedElement();
            dataManager.movedElement(selectedShape, x,y);
            
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(m3State.DRAGGING_NOTHING)) {
            dataManager.setState(SELECTING_ELEMENT);
        }
    }
    
    /**
     * Respond to mouse double click on the rendering surface, which we call canvas,
     * but is actually a ScrollPane.
     * @param x mouse x coordinate
     * @param y mouse x coordinate
     */
    public void processCanvasMouseDoubleClick(int x, int y) {
    }
}
