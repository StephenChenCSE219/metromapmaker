package m3.gui;

import djf.AppTemplate;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import m3.data.MetroLine;
import m3.data.MetroStation;
import m3.data.m3Data;
import m3.data.m3State;

/**
 * This class responds to interactions with other UI logo editing controls.
 * 
 * @author Stephen X Chen
 * @version 1.0
 */
public class m3Controller {
    
    AppTemplate app;
    m3Data dataManager;
    
    public m3Controller(AppTemplate initApp) {
        app = initApp;
	dataManager = (m3Data)app.getDataComponent();
    }
    
    /**
     * This method handles a user request to add a station.
     */
    public void processAddStation(){
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.CROSSHAIR);
	
	// CHANGE THE STATE
	dataManager.setState(m3State.STARTING_STATION);

	// ENABLE/DISABLE THE PROPER BUTTONS
	m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * This method handles a user request to add a line.
     */
    public void processAddLine(){
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.CROSSHAIR);
	
	// CHANGE THE STATE
	dataManager.setState(m3State.STARTING_LINE);

	// ENABLE/DISABLE THE PROPER BUTTONS
	m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }
    
    public void processListStations(){
        dataManager.listCurrentLineStations();
    }
    
    /**
     * This method handles a user request to add a label.
     */
    public void processAddLabel(){        
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.CROSSHAIR);
	
	// CHANGE THE STATE
	dataManager.setState(m3State.STARTING_LABEL);

	// ENABLE/DISABLE THE PROPER BUTTONS
	m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * This method handles a user request to add an image.
     */
    public void processAddImage(){
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.CROSSHAIR);
	
	// CHANGE THE STATE
	dataManager.setState(m3State.STARTING_IMAGE);

	// ENABLE/DISABLE THE PROPER BUTTONS
	m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }
   
    /**
     * This method handles a user request to use the selection tool.
     */
    public void processSelectSelectionTool(){
    }
    
    /**
     * This method handles a user request to remove the selected element.
     */
    public void processRemoveSelectedElement(){
        dataManager.removeSelectedElement();
    }
    
    /**
     * This method handles a user request to create a line.
     */
    public void processSelectLineToDraw(){
    }
    
    /**
     * This method handles a user request to create a station.
     */
    public void processSelectStationToDraw(){
    }
    
    /**
     * This method handles a user request to move the selected element to the back.
     */
    public void processMoveSelectedElementToBack(){
    }
    
    /**
     * This method handles a user request to move the selected element to the front.
     */
    public void processMoveSelectedElementToFront(){
    }
    
    /**
     * This method handles a user request to select a font.
     * @param s selected font
     */
    public void processSelectFont(String s){
        dataManager.setFont(s);
    }
    
    public void processSelectSize(int i){
        dataManager.setFontSize(i);
    }
    
    /**
     * This method handles a user request to bold text.
     */
    public void processSelectBold(){
        dataManager.toggleBoldText();
    }
    
    /**
     * This method handles a user request to italicize text.
     */
    public void processSelectItalics(){
        dataManager.toggleItalicizeText();
    }
    
    public void processSelectFontColor(Color c){
        dataManager.setCurrentElementFillColor(false, c);
    }
    
    /**
     * This method handles a user request to select a fill for an element.
     */
    public void processSelectFillColor(){
    }
    
    /**
     * This method handles a user request to select a fill for the background.
     */
    public void processBackgroundFill(Background bg, Pane p){
        dataManager.setBackground(bg, p);
    }
    
    public void processBackgroundFill(String file, Pane p){
        dataManager.setBackground(file, p);
    }
    
    /**
     * This method handles a user request to select an image for the background.
     */
    public void processSelectBackgroundImage(){
    }
    
    /**
     * This method handles a user request to select a line thickness.
     */
    public void processSelectLineThickness(){
    }
    
    /**
     * This method handles a user request to export the map.
     */
    public void processSnapshot(){
    }
    
    /**
     * This method handles a user request to zoom the map.
     */
    public void processZoom(){
    }
    
    /**
     * This method handles a user request to find a route between two stations.
     */
    public void processFindRoute(){
    }
    
    /**
     * This method handles a user request to undo the last action.
     */
    public void processUndo(){
        dataManager.undo();
	m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * This method handles a user request to redo the previous action.
     */
    public void processRedo(){
        dataManager.redo();
	m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * This method handles a user request to add a station to a line.
     */
    public void processAddStationToLine(){
        // CHANGE THE CURSOR
	Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.HAND);
	
	// CHANGE THE STATE
	dataManager.setState(m3State.SELECTING_STATION_TO_ADD);	
	
	// ENABLE/DISABLE THE PROPER BUTTONS
	m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * This method handles a user request to add a label to a line/station.
     */
    public void processAddLabelToElement(){
    }
    
    /**
     * This method handles a user request to move a line end.
     */
    public void processMoveLineEnd(){
    }
    
    /**
     * This method handles a user request to move a line end.
     */
    public void processRemoveAllStations(){
    }
    
    public void processSnapStation(){
        dataManager.snapStation();
    }
    
    /**
     * This method handles a user request to toggle the snap grid.
     */
    public void processToggleGrid(){
    }

    public void processSelectLine(MetroLine line) {
        dataManager.selectLine(line);
    }

    public void processSelectStation(int i) {
        dataManager.selectStation(i);
    }

    public void processEditLine() {
        dataManager.editSelectedLine();
        //dataManager.updateDisplayLine(s);
    }

    public void processRemoveStationFromLine() {
        // CHANGE THE CURSOR
	Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.HAND);
	
	// CHANGE THE STATE
	dataManager.setState(m3State.SELECTING_STATION_TO_REMOVE);	
	
	// ENABLE/DISABLE THE PROPER BUTTONS
	m3Workspace workspace = (m3Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }

    public void processRemoveStation() {
        dataManager.removeStation();
    }

    void processFillStation(Color value) {
        dataManager.setCurrentElementFillColor(true, value);
    }

    void processRemoveLine() {
        dataManager.removeSelectedLine();
    }

    void processExport() {
        try {
            app.getFileComponent().exportData(dataManager, "work/" + dataManager.getMapName());
        } catch (IOException ex) {
        }
    }

    void processChangeLineThickness(double value) {
        dataManager.setCurrentLineThickness(value);
    }

    void processLineThicknessPressed(double value) {
        dataManager.setLineThicknessStartPos(value);
    }

    void processLineThicknessReleased(double value) {
        dataManager.setLineThicknessEndPos(value);
        dataManager.setLineThickness(value);
        }

    void processChangeStationRadius(double value) {
        dataManager.setCurrentCircleRadius(value);
    }

    void processStationRadiusPressed(double value) {
        dataManager.setCircleRadiusStartPos(value);
    }

    void processStationRadiusReleased(double value) {
        dataManager.setCircleRadiusEndPos(value);
        dataManager.setCircleRadius(value);
    }

    void processFindRoute(MetroStation s, MetroStation s1) {
        dataManager.findRoute(s, s1);
    }

    void processMoveLabel() {
        dataManager.moveSelectedStationLabel();
    }
    
    void processRotateLabel() {
        dataManager.rotateSelectedStationLabel();
    }

    void setChangeMapSize(double d) {
        dataManager.changeMapSize(d);
    }
}
