package m3.gui;

import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import static m3.m3LanguageProperty.*;
import m3.data.m3Data;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppPropertyType.APP_LOGO;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import static m3.css.m3Style.*;
import m3.data.MetroElement;
import m3.data.MetroLine;
import m3.data.MetroStation;
import properties_manager.PropertiesManager;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author Stephen Chen
 * @version 1.0
 */
public class m3Workspace extends AppWorkspaceComponent {
    
    // THIS IS WHERE WE'LL RENDER OUR DRAWING, NOTE THAT WE
    // CALL THIS A CANVAS, BUT IT'S REALLY JUST A Pane
    ScrollPane canvas;
    Pane map;
    Pane grid;
    Pane background;
    StackPane stack;
    double zoomLevel;
    double mapSize;
    Group mapObjects;
    Group outerGroup;
    
    //public ObservableList<String> displayLines;
    //public ObservableList<String> displayStations;
    
    // HERE ARE THE CONTROLLERS
    CanvasController canvasController;
    m3Controller metroController;    

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    AppTemplate app;
    AppGUI gui;

    Button exportButton;
    
    HBox undoRedoToolbar;
    Button undoButton;
    Button redoButton;

    HBox aboutToolbar;
    Button aboutButton;
    
    VBox editToolbar;

    VBox linesToolbar;
    HBox linesRow;
    Label linesLabel;
    public ComboBox<MetroLine> linesComboBox;
    public Button lineColor;
    HBox linesButtons;
    Button addLineButton;
    Button removeLineButton;
    Button editLineButton;
    Button moveLineButton;
    Button addStationToLineButton;
    Button removeStationFromLineButton;
    Button listStationsButton;
    public Slider lineThickness;

    VBox stationsToolbar;
    HBox stationsRow;
    Label stationsLabel;
    public ComboBox<MetroStation> stationsComboBox;
    public ColorPicker stationFill;
    HBox stationsButtons;
    Button addStationButton;
    Button removeStationButton;
    Button snapButton;
    Button moveLabelButton;
    Button rotateLabelButton;
    public Slider stationCircleRadius;

    HBox routeToolbar;
    VBox routeStationSelector;
    ComboBox<MetroStation> stationComboBoxOrigin;
    ComboBox<MetroStation> stationComboBoxDest;
    Button findRouteButton;

    VBox decorToolbar;
    Label decorLabel;
    HBox decorRow;
    HBox decorButtons;
    Button setImageButton;
    ColorPicker setBackgroundFill;
    Button addImageButton;
    Button addLabelButton;
    Button moveElementButton;
    Button removeElementButton;

    VBox fontToolbar;
    HBox fontRow;
    Label fontLabel;
    HBox fontButtons;
    ColorPicker textColor;
    ComboBox<String> textFont;
    ComboBox<Integer> textSize;
    Button boldButton;
    Button italicsButton;

    VBox navToolbar;
    HBox navRow;
    Label navLabel;
    HBox navButtons;
    Button zoomInButton;
    Button zoomOutButton;
    Button increaseMapButton;
    Button decreaseMapButton;

    CheckBox snapGrid;
    
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public m3Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }
        
    // HELPER SETUP METHOD
    private void initLayout() {
        
        exportButton = gui.initChildButton(gui.getFileToolbar(), EXPORT_ICON.toString(), EXPORT_TOOLTIP.toString(), false);
        
        undoRedoToolbar = new HBox();
        Separator undoRedoSeparator = new Separator();
        undoRedoSeparator.setOrientation(Orientation.VERTICAL);
        undoRedoSeparator.setMinWidth(20);
        undoRedoToolbar.getChildren().add(undoRedoSeparator);
        undoButton = gui.initChildButton(undoRedoToolbar, UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), false);
        redoButton = gui.initChildButton(undoRedoToolbar, REDO_ICON.toString(), REDO_TOOLTIP.toString(), false);
        gui.getFileToolbar().getChildren().add(undoRedoToolbar);
        
        aboutToolbar = new HBox();
        Separator aboutSeparator = new Separator();
        aboutSeparator.setOrientation(Orientation.VERTICAL);
        aboutSeparator.setMinWidth(20);
        aboutToolbar.getChildren().add(aboutSeparator);
        aboutButton = gui.initChildButton(aboutToolbar, INFO_ICON.toString(), INFO_TOOLTIP.toString(), false);
        gui.getFileToolbar().getChildren().add(aboutToolbar);
        
        // THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
	editToolbar = new VBox();
	
        linesToolbar = new VBox();
        linesRow = new HBox(25);
        linesLabel = new Label("Metro Lines");
        //displayLines = FXCollections.observableArrayList();
        linesComboBox = new ComboBox<>(/*displayLines*/);
        linesComboBox.setPrefWidth(160);
        lineColor = new Button();
        lineColor.setBackground(new Background(new BackgroundFill(Color.BLACK, new CornerRadii(15), null)));
        lineColor.setText(((Color)(lineColor.getBackground().getFills().get(0).getFill())).toString());
        lineColor.setTextFill(((Color)(lineColor.getBackground().getFills().get(0).getFill())).invert());
        lineColor.setPrefWidth(80);
        linesToolbar.getChildren().add(linesLabel);
        linesRow.getChildren().add(linesComboBox);
        linesRow.getChildren().add(lineColor);
        linesButtons = new HBox();
        linesButtons.setAlignment(Pos.CENTER_LEFT);
        addLineButton = gui.initChildButton(linesToolbar, ADD_ICON.toString(), ADD_TOOLTIP.toString(), false);
        removeLineButton = gui.initChildButton(linesToolbar, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), false);
        //moveLineButton = gui.initChildButton(linesToolbar, INFO_ICON.toString(), INFO_TOOLTIP.toString(), false);
        addStationToLineButton = initChildButton(linesToolbar, "Add Station", "Add Station", false);
        addStationToLineButton.setPrefWidth(70);
        removeStationFromLineButton = initChildButton(linesToolbar, "Remove Station", "Remove Station", false);
        removeStationFromLineButton.setPrefWidth(70);
        listStationsButton = gui.initChildButton(linesToolbar, LIST_ICON.toString(), LIST_TOOLTIP.toString(), false);
        linesButtons.getChildren().add(addLineButton);
        linesButtons.getChildren().add(removeLineButton);
        //linesButtons.getChildren().add(moveLineButton);
        linesButtons.getChildren().add(addStationToLineButton);
        linesButtons.getChildren().add(removeStationFromLineButton);
        linesButtons.getChildren().add(listStationsButton);
        lineThickness = new Slider(2, 15, 6);
        linesToolbar.getChildren().add(linesRow);
        linesToolbar.getChildren().add(linesButtons);
        linesToolbar.getChildren().add(lineThickness);
        editToolbar.getChildren().add(linesToolbar);
        
        stationsToolbar = new VBox();
        stationsRow = new HBox(25);
        stationsLabel = new Label("Metro Stations");
        //displayStations = FXCollections.observableArrayList();
        stationsComboBox = new ComboBox<>(/*displayStations*/);
        stationsComboBox.setPrefWidth(140);
        stationFill = new ColorPicker(Color.WHITE);
        stationFill.setPrefWidth(100);
        stationsToolbar.getChildren().add(stationsLabel);
        stationsRow.getChildren().add(stationsComboBox);
        stationsRow.getChildren().add(stationFill);
        stationsButtons = new HBox();
        stationsButtons.setAlignment(Pos.CENTER_LEFT);
        addStationButton = gui.initChildButton(aboutToolbar, ADD_ICON.toString(), ADD_TOOLTIP.toString(), false);
        removeStationButton = gui.initChildButton(aboutToolbar, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), false);
        snapButton = initChildButton(aboutToolbar, "Snap to Grid", "Snap to Grid", false);
        snapButton.setPrefWidth(70);
        moveLabelButton = initChildButton(aboutToolbar, "Move Label", "Move Label", false);
        moveLabelButton.setPrefWidth(70);
        rotateLabelButton = gui.initChildButton(aboutToolbar, ROTATE_ICON.toString(), ROTATE_TOOLTIP.toString(), false);
        stationsButtons.getChildren().add(addStationButton);
        stationsButtons.getChildren().add(removeStationButton);
        stationsButtons.getChildren().add(snapButton);
        stationsButtons.getChildren().add(moveLabelButton);
        stationsButtons.getChildren().add(rotateLabelButton);
        stationCircleRadius = new Slider(5, 20, 7);
        stationsToolbar.getChildren().add(stationsRow);
        stationsToolbar.getChildren().add(stationsButtons);
        stationsToolbar.getChildren().add(stationCircleRadius);
        editToolbar.getChildren().add(stationsToolbar);
        
        routeToolbar = new HBox();
        routeToolbar.setAlignment(Pos.CENTER);
        routeStationSelector = new VBox();
        stationComboBoxOrigin = new ComboBox<>();
        stationComboBoxOrigin.setPrefWidth(200);
        stationComboBoxDest = new ComboBox<>();
        stationComboBoxDest.setPrefWidth(200);
        routeStationSelector.getChildren().add(stationComboBoxOrigin);
        routeStationSelector.getChildren().add(stationComboBoxDest);
        routeToolbar.getChildren().add(routeStationSelector);
        findRouteButton = gui.initChildButton(routeToolbar, TRAIN_ICON.toString(), TRAIN_TOOLTIP.toString(), false);
        editToolbar.getChildren().add(routeToolbar);
        
        decorToolbar = new VBox();
        decorRow = new HBox(180);
        decorRow.setAlignment(Pos.CENTER_LEFT);
        decorLabel = new Label("Decor");
        decorRow.getChildren().add(decorLabel);
        decorButtons = new HBox();
        setImageButton = initChildButton(decorButtons, "Set Image\nBackground", "Set Image Background", false);
        setBackgroundFill = new ColorPicker(Color.WHITE);
        setBackgroundFill.setPrefWidth(40);
        decorRow.getChildren().add(setBackgroundFill);
        addImageButton = initChildButton(decorButtons, "Add\nImage", "Add Image", false);
        addLabelButton = initChildButton(decorButtons, "Add\nLabel", "Add Label", false);
        //moveElementButton = gui.initChildButton(decorButtons, INFO_ICON.toString(), INFO_TOOLTIP.toString(), false);
        removeElementButton = initChildButton(decorButtons, "Remove\nElement", "Remove Element", false);
        decorToolbar.getChildren().add(decorRow);
        decorToolbar.getChildren().add(decorButtons);
        editToolbar.getChildren().add(decorToolbar);
        
        ObservableList<String> fonts = 
            FXCollections.observableArrayList(
            "Arial",
            "Georgia",
            "Monospaced",
            "Times New Roman",
            "Verdana"
        );
        ObservableList<Integer> sizes = 
            FXCollections.observableArrayList(
            8,9,10,11,12,14,16,18,24,30,36,42,48,54,60,72,84,96,120,144
        );
        
        fontToolbar = new VBox();
        fontRow = new HBox();
        fontLabel = new Label("Font");
        textColor = new ColorPicker(Color.BLACK);
        textColor.setPrefWidth(40);
        fontRow.getChildren().add(fontLabel);
        //fontRow.getChildren().add(textColor);
        fontToolbar.getChildren().add(fontRow);
        fontButtons = new HBox(4);
        fontButtons.setAlignment(Pos.CENTER_LEFT);
        fontButtons.getChildren().add(textColor);
        boldButton = gui.initChildButton(fontButtons, BOLD_ICON.toString(), BOLD_TOOLTIP.toString(), false);
        italicsButton = gui.initChildButton(fontButtons, ITALICS_ICON.toString(), ITALICS_TOOLTIP.toString(), false);
        textFont = new ComboBox<>(fonts);
        textFont.setPrefWidth(100);
        textFont.getSelectionModel().select("Arial");
        textSize = new ComboBox<>(sizes);
        textSize.getSelectionModel().select((Integer)(12));
        fontButtons.getChildren().add(textFont);
        fontButtons.getChildren().add(textSize);
        fontToolbar.getChildren().add(fontButtons);
        editToolbar.getChildren().add(fontToolbar);
        
        navToolbar = new VBox();
        navRow = new HBox(105);
        navRow.setAlignment(Pos.CENTER_LEFT);
        navLabel = new Label("Navigation");
        VBox cB = new VBox();
        cB.setAlignment(Pos.CENTER);
        Text checkBox = new Text("Show Grid ");
        snapGrid = new CheckBox();
        cB.getChildren().add(checkBox);
        cB.getChildren().add(snapGrid);
        navRow.getChildren().add(navLabel);
        navRow.getChildren().add(cB);
        navButtons = new HBox(34);
        zoomInButton = gui.initChildButton(navButtons, ZOOMIN_ICON.toString(), ZOOMIN_TOOLTIP.toString(), false);
        zoomOutButton = gui.initChildButton(navButtons, ZOOMOUT_ICON.toString(), ZOOMOUT_TOOLTIP.toString(), false);
        increaseMapButton = gui.initChildButton(navButtons, INCREASE_ICON.toString(), INCREASE_TOOLTIP.toString(), false);
        decreaseMapButton = gui.initChildButton(navButtons, DECREASE_ICON.toString(), DECREASE_TOOLTIP.toString(), false);
        navToolbar.getChildren().add(navRow);
        navToolbar.getChildren().add(navButtons);
        editToolbar.getChildren().add(navToolbar);
        
	// WE'LL RENDER OUR STUFF HERE IN THE CANVAS
	canvas = new ScrollPane();
        mapObjects = new Group();
        map = new Pane();
        grid = new Pane();
        background = new Pane();
        stack = new StackPane();
        zoomLevel = 1.0;
        mapSize = 1.0;
        
        try {
            Image img = SwingFXUtils.toFXImage(ImageIO.read(new File("images/gridlines.png")), null);
            grid.setBackground(new Background(new BackgroundImage(img, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT)));
        } catch (IOException ex) {
        }
        grid.setOpacity(0);
        
        background.setPrefSize(1520, 855);
        
        canvas.prefWidthProperty().bind(map.widthProperty());
        canvas.prefHeightProperty().bind(map.heightProperty());
        //map.prefWidthProperty().bind(canvas.widthProperty());
        //map.prefHeightProperty().bind(canvas.heightProperty());
        grid.prefWidthProperty().bind(map.widthProperty());
        grid.prefHeightProperty().bind(map.heightProperty());
        map.prefWidthProperty().bind(background.widthProperty());
        map.prefHeightProperty().bind(background.heightProperty());
        background.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
        stack.getChildren().add(background);
        stack.getChildren().add(grid);
        stack.getChildren().add(map);
        mapObjects.getChildren().add(stack);
        outerGroup = new Group(mapObjects);
        canvas.setContent(outerGroup);
	
	// AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
	m3Data data = (m3Data)app.getDataComponent();
	data.setElements(map.getChildren());

	// AND NOW SETUP THE WORKSPACE
	workspace = new BorderPane();
	((BorderPane)workspace).setLeft(editToolbar);
	((BorderPane)workspace).setCenter(canvas);
    }
    
    // HELPER SETUP METHOD
    private void initControllers() {
        m3Data data = (m3Data)app.getDataComponent();
	metroController = new m3Controller(app);
        
        
        aboutButton.setOnAction(e->{
            PropertiesManager props = PropertiesManager.getPropertiesManager();
	    Alert alert = new Alert(AlertType.INFORMATION, "", ButtonType.CLOSE);
            String appIcon = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO);
            alert.setGraphic(new ImageView(new Image(appIcon)));
            alert.setTitle("MetroMapMaker Info");
            alert.setHeaderText("App:\n Metro Map Maker\n\nAuthor:\n Stephen X Chen, 2017");
            alert.showAndWait();
	});
        
        addStationButton.setOnAction(e->{
	    metroController.processAddStation();
	});
        removeStationButton.setOnAction(e->{
	    metroController.processRemoveStation();
	});
        
        addLineButton.setOnAction(e->{
	    metroController.processAddLine();
	});
        removeLineButton.setOnAction(e->{
	    metroController.processRemoveLine();
	});
        
        addStationToLineButton.setOnAction(e->{
	    metroController.processAddStationToLine();
	});
        removeStationFromLineButton.setOnAction(e->{
	    metroController.processRemoveStationFromLine();
	});
        
        listStationsButton.setOnAction(e->{
	    metroController.processListStations();
	});
        
        lineThickness.valueProperty().addListener(e-> {
	    metroController.processChangeLineThickness(lineThickness.getValue());
	});
        
        lineThickness.setOnMousePressed(e-> {
	    metroController.processLineThicknessPressed(lineThickness.getValue());
	});
        lineThickness.setOnMouseReleased(e-> {
	    metroController.processLineThicknessReleased(lineThickness.getValue());
	});
        
        stationCircleRadius.valueProperty().addListener(e-> {
	    metroController.processChangeStationRadius(stationCircleRadius.getValue());
	});
        
        stationCircleRadius.setOnMousePressed(e-> {
	    metroController.processStationRadiusPressed(stationCircleRadius.getValue());
	});
        stationCircleRadius.setOnMouseReleased(e-> {
	    metroController.processStationRadiusReleased(stationCircleRadius.getValue());
	});
        
        addImageButton.setOnAction(e-> {
	    metroController.processAddImage();
	});
        addLabelButton.setOnAction(e-> {
	    metroController.processAddLabel();
	});
        
        moveLabelButton.setOnAction(e-> {
	    metroController.processMoveLabel();
	});
        
        rotateLabelButton.setOnAction(e-> {
	    metroController.processRotateLabel();
	});
        
        snapGrid.selectedProperty().addListener(e->{
            if(grid.getOpacity() == 0)
                grid.setOpacity(255);
            else
                grid.setOpacity(0);
        });
        
        zoomInButton.setOnAction(e->{
            zoomLevel += .1;
            mapObjects.setScaleX(zoomLevel);
            mapObjects.setScaleY(zoomLevel);
        });
        zoomOutButton.setOnAction(e->{
            zoomLevel -= .1;
            mapObjects.setScaleX(zoomLevel);
            mapObjects.setScaleY(zoomLevel);
        });
        
        increaseMapButton.setOnAction(e->{
            metroController.setChangeMapSize(.1);
        });
        decreaseMapButton.setOnAction(e->{
            if(mapSize * 855 > 200)
                metroController.setChangeMapSize(-.1);
        });
        
        linesComboBox.setOnAction(e->{ 
	    metroController.processSelectLine(linesComboBox.getSelectionModel().getSelectedItem());
	});
        
        linesComboBox.setCellFactory((ListView<MetroLine> line) -> {
            ListCell<MetroLine> cell = new ListCell<MetroLine>(){
                @Override
                protected void updateItem(MetroLine line, boolean empty){
                    super.updateItem(line, empty);
                    if(empty){
                        setText("");
                    }
                    else{
                        setText(line.toString());
                    }
                }
            };
            return cell;
        });
        linesComboBox.setButtonCell(
                new ListCell<MetroLine>(){
                    @Override
                    protected void updateItem(MetroLine line, boolean bln){
                        super.updateItem(line, bln);
                        if(bln){
                            setText("");
                        }
                        else{
                            setText(line.toString());
                        }
                    }
                }
        );
        
        stationsComboBox.setOnAction(e->{ 
	    //metroController.processSelectStation(stationsComboBox.getItems().indexOf(stationsComboBox.getSelectionModel().getSelectedItem()));
            //metroController.processSelectStation(stationsComboBox.getSelectionModel().getSelectedIndex());
	});
        
        stationsComboBox.setCellFactory((ListView<MetroStation> station) -> {
            ListCell<MetroStation> cell = new ListCell<MetroStation>(){
                @Override
                protected void updateItem(MetroStation station, boolean empty){
                    super.updateItem(station, empty);
                    if(empty){
                        setText("");
                    }
                    else{
                        setText(station.toString());
                    }
                }
            };
            return cell;
        });
        stationsComboBox.setButtonCell(
                new ListCell<MetroStation>(){
                    @Override
                    protected void updateItem(MetroStation station, boolean bln){
                        super.updateItem(station, bln);
                        if(bln){
                            setText("");
                        }
                        else{
                            setText(station.toString());
                        }
                    }
                }
        );
        
        stationComboBoxOrigin.setCellFactory((ListView<MetroStation> station) -> {
            ListCell<MetroStation> cell = new ListCell<MetroStation>(){
                @Override
                protected void updateItem(MetroStation station, boolean empty){
                    super.updateItem(station, empty);
                    if(empty){
                        setText("");
                    }
                    else{
                        setText(station.toString());
                    }
                }
            };
            return cell;
        });
        stationComboBoxOrigin.setButtonCell(
                new ListCell<MetroStation>(){
                    @Override
                    protected void updateItem(MetroStation station, boolean bln){
                        super.updateItem(station, bln);
                        if(bln){
                            setText("");
                        }
                        else{
                            setText(station.toString());
                        }
                    }
                }
        );
        
        stationComboBoxDest.setCellFactory((ListView<MetroStation> station) -> {
            ListCell<MetroStation> cell = new ListCell<MetroStation>(){
                @Override
                protected void updateItem(MetroStation station, boolean empty){
                    super.updateItem(station, empty);
                    if(empty){
                        setText("");
                    }
                    else{
                        setText(station.toString());
                    }
                }
            };
            return cell;
        });
        stationComboBoxDest.setButtonCell(
                new ListCell<MetroStation>(){
                    @Override
                    protected void updateItem(MetroStation station, boolean bln){
                        super.updateItem(station, bln);
                        if(bln){
                            setText("");
                        }
                        else{
                            setText(station.toString());
                        }
                    }
                }
        );
        
        setBackgroundFill.setOnAction(e->{ 
	    metroController.processBackgroundFill(new Background(new BackgroundFill(setBackgroundFill.getValue(), null, null)), background);
	});
        
        setImageButton.setOnAction(e->{
            
            FileChooser fc = new FileChooser();

            ArrayList<String> imageExtensions = new ArrayList<>();
            imageExtensions.add("*.JPG");
            imageExtensions.add("*.JPEG");
            imageExtensions.add("*.PNG");
            imageExtensions.add("*.GIF");
            imageExtensions.add("*.BMP");
            FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Image Files", imageExtensions);
            fc.getExtensionFilters().add(filter);

            File file = fc.showOpenDialog(null);

            if (file != null) {
                try {
                    Image img = SwingFXUtils.toFXImage(ImageIO.read(file), null);
                    metroController.processBackgroundFill(file.getPath(), background);
                } catch (IOException ex) {
                }
            }
            
            
	    //metroController.processBackgroundFill();
	});
        
        lineColor.setOnAction(e->{ 
	    metroController.processEditLine();
	});
        stationFill.setOnAction(e->{ 
	    metroController.processFillStation(stationFill.getValue());
	});
        
        snapButton.setOnAction(e->{ 
	    metroController.processSnapStation();
	});
        
        exportButton.setOnAction(e->{ 
            m3Data mdata = (m3Data)app.getDataComponent();
            mdata.loadMapName();
            mdata.setExportImage(stack.snapshot(new SnapshotParameters(), null));
	    metroController.processExport();
	});
        
        undoButton.setOnAction(e -> {
            metroController.processUndo();
        });
        redoButton.setOnAction(e -> {
            metroController.processRedo();
        });
        
        textFont.setOnAction(e->{ 
	    metroController.processSelectFont(textFont.getValue());
	});
        textSize.setOnAction(e->{ 
	    metroController.processSelectSize(textSize.getValue());
	});
        boldButton.setOnAction(e->{ 
	    metroController.processSelectBold();
	});
        italicsButton.setOnAction(e->{ 
	    metroController.processSelectItalics();
	});
        textColor.setOnAction(e->{ 
	    metroController.processSelectFontColor(textColor.getValue());
	});
        
        findRouteButton.setOnAction(e->{
            if(stationComboBoxOrigin.getValue() != null && stationComboBoxDest.getValue() != null)
                metroController.processFindRoute(stationComboBoxOrigin.getValue(), stationComboBoxDest.getValue());
	});
        
        removeElementButton.setOnAction(e->{ 
	    metroController.processRemoveSelectedElement();
	});
        
        canvas.setOnKeyPressed((KeyEvent k) -> {
            switch(k.getCode()) {
                case W:
                    canvas.setVvalue(canvas.getVvalue() - 0.1);
                    break;
                case S:
                    canvas.setVvalue(canvas.getVvalue() + 0.1);
                    break;
                case A:
                    canvas.setHvalue(canvas.getHvalue() - 0.1);
                    break;
                case D:
                    canvas.setHvalue(canvas.getHvalue() + 0.1);
                    break;
            }
        });
        
	// MAKE THE CANVAS CONTROLLER	
	canvasController = new CanvasController(app);
        //canvas.setOnMouseClicked(e->{
        //    if (e.getClickCount() == 2)
        //        canvasController.processCanvasMouseDoubleClick((int)e.getX(), (int)e.getY());
	//});
	map.setOnMousePressed(e->{
	    canvasController.processCanvasMousePress((int)e.getX(), (int)e.getY());
	});
	map.setOnMouseReleased(e->{
	    canvasController.processCanvasMouseRelease((int)e.getX(), (int)e.getY());
	});
	map.setOnMouseDragged(e->{
	    canvasController.processCanvasMouseDragged((int)e.getX(), (int)e.getY());
	});
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamically as the application
     * runs so they will have their style setup separately.
     */
    public void initStyle(){
	editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        linesToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        stationsToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        routeToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        decorToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        fontToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        navToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        
	linesLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	stationsLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	decorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	fontLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	navLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
    }

    /**
     * This function sets the relevant user input controls to the properties of
     * the selected element. For example, when a label is clicked, the font box
     * changes to match the font of the label text.
     * @param e the metro element whose settings need to be applied
     */
    public void loadSelectedElementSettings(MetroElement e){
    }
    
    /**
     * Sets the map view (a ViewPort) to the specified location
     * @param x center x coordinate
     * @param y center y coordinate
     * @param zoom zoom level
     */
    public void setMapView(int x, int y, int zoom){
    }
    
    /**
     * Not used.
     */
    @Override
    public void resetWorkspace() {
    }

    /**
     * This function reloads all the controls for editing logos
     * the workspace.
     */
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
    }
    
    private Button initChildButton(Pane toolbar, String text, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setText(text);
        button.setFont(Font.font("Times New Roman", FontWeight.BOLD, 14));
        button.setWrapText(true);
        button.setTextAlignment(TextAlignment.CENTER);
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }

    public Pane getCanvas() {
        return map;
    }

    public void addStationToComboBox(MetroStation metroStation) {
        stationsComboBox.getItems().add(metroStation);
        stationsComboBox.setValue(metroStation);
        
        stationComboBoxOrigin.getItems().add(metroStation);
        stationComboBoxDest.getItems().add(metroStation);
        //this.updateEditLineButton();
    }
    
    public void addLineToComboBox(MetroLine metroLine) {
        linesComboBox.getItems().add(metroLine);
        linesComboBox.setValue(metroLine);
        //this.updateEditLineButton();
    }

    public void setBackground(Background newBackground) {
        background.setBackground(newBackground);
        if(newBackground.getFills().size()>0)
            setBackgroundFill.setValue((Color)newBackground.getFills().get(0).getFill());
        else
            setBackgroundFill.setValue(null);
    }
    
    public void setMapSize(double i){
            mapSize += i;
            background.setMinSize(1520 * mapSize, 855 * mapSize);
            background.setPrefSize(1520 * mapSize, 855 * mapSize);
            background.setMaxSize(1520 * mapSize, 855 * mapSize);
    }

    public Pane getBackground() {
        return background;
    }

    public void removeLineFromComboBox(MetroLine selectedLine) {
        linesComboBox.getItems().remove(selectedLine);
        linesComboBox.setValue(null);
    }

    public void removeStationFromComboBox(MetroStation toRemove) {
        stationsComboBox.getItems().remove(toRemove);
        stationsComboBox.setValue(null);
    }
}
