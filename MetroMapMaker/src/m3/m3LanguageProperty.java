package m3;

/**
 * This class provides the properties that are needed to be loaded from
 * language-dependent XML files.
 * 
 * @author Stephen Chen
 * @version 1.0
 */
public enum m3LanguageProperty {
    APP_LOGO,
    APP_ICON,
    
    NEW_TOOLTIP,
    LOAD_TOOLTIP,
    SAVE_TOOLTIP,
    EXIT_TOOLTIP,
    
    EXPORT_ICON,
    EXPORT_TOOLTIP,
    
    ADD_ICON,
    ADD_TOOLTIP,
    
    UNDO_ICON,
    UNDO_TOOLTIP,
    REDO_ICON,
    REDO_TOOLTIP,
    
    LANGUAGE_ICON,
    LANGUAGE_TOOLTIP,
    
    INFO_ICON,
    INFO_TOOLTIP,
    
    SAVEAS_ICON,
    SAVEAS_TOOLTIP,
    
    SELECTION_TOOL_ICON,
    SELECTION_TOOL_TOOLTIP,
    
    REMOVE_ICON,
    REMOVE_TOOLTIP,
    
    IMAGE_ICON,
    IMAGE_TOOLTIP,
    
    TEXT_ICON,
    TEXT_TOOLTIP,
    
    BOLD_ICON,
    BOLD_TOOLTIP,
    
    ITALICS_ICON,
    ITALICS_TOOLTIP,
    
    LIST_ICON,
    LIST_TOOLTIP,
    
    TRAIN_ICON,
    TRAIN_TOOLTIP,
    
    DECREASE_ICON,
    DECREASE_TOOLTIP,
    
    ZOOMIN_ICON,
    ZOOMIN_TOOLTIP,
    
    ZOOMOUT_ICON,
    ZOOMOUT_TOOLTIP,
    
    INCREASE_ICON,
    INCREASE_TOOLTIP,
    
    BACKGROUND_TOOLTIP,
    FILL_TOOLTIP,
    OUTLINE_TOOLTIP,
    
    SNAPSHOT_ICON,
    SNAPSHOT_TOOLTIP,
    
    ROTATE_ICON,
    ROTATE_TOOLTIP
}