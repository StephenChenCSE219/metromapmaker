package m3.tps;

import jtps.jTPS_Transaction;
import m3.data.MetroLabel;
import m3.data.m3Data;

public class AddLabel_Transaction implements jTPS_Transaction{

    m3Data data;
    MetroLabel label;
    
    public AddLabel_Transaction(MetroLabel initLabel, m3Data initData) {
        data = initData;
        label = initLabel;
    }
    
    @Override
    public void doTransaction() {
        data.getElements().add(label);
        data.setSelectedElement(label);
    }

    @Override
    public void undoTransaction() {
        data.getElements().remove(label);
        data.setSelectedElement(null);
    }
    
    @Override
    public String toString() {
        return "";
    }
}
