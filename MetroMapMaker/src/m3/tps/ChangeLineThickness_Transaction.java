package m3.tps;

import javafx.scene.control.Slider;
import jtps.jTPS_Transaction;
import m3.data.MetroLine;

public class ChangeLineThickness_Transaction implements jTPS_Transaction{

    private Slider slider;
    double oldPos;
    double newPos;
    MetroLine line;
    
    public ChangeLineThickness_Transaction(MetroLine initLine, Slider initSlider, double oPos, double nPos) {
        slider = initSlider;
        line = initLine;
        oldPos = oPos;
        newPos = nPos;
    }
    
    @Override
    public void doTransaction() {
        slider.setValue(newPos);
        line.setThickness(newPos);
    }

    @Override
    public void undoTransaction() {
        slider.setValue(oldPos);
        line.setThickness(oldPos);
    }
    
    @Override
    public String toString() {
        return "Line Thickness";
    }
}
