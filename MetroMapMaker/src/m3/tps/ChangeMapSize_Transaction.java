/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m3.tps;

import jtps.jTPS_Transaction;
import m3.gui.m3Workspace;

/**
 *
 * @author sxc
 */
public class ChangeMapSize_Transaction implements jTPS_Transaction {
    
    double dbl;
    m3Workspace work;

    public ChangeMapSize_Transaction(double d, m3Workspace wrk) {
        dbl = d;
        work = wrk;
    }

    @Override
    public void doTransaction() {
        work.setMapSize(dbl);
    }

    @Override
    public void undoTransaction() {
        work.setMapSize(-dbl);
    }
    
}
