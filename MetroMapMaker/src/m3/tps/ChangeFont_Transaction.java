package m3.tps;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;
import m3.data.MetroLabel;

public class ChangeFont_Transaction implements jTPS_Transaction{

    MetroLabel label;
    String font;
    int size;
    boolean toggleBold;
    boolean toggleItalics;
    Font oldFont;
    
    public ChangeFont_Transaction(MetroLabel initLabel, String initFont, int initSize, boolean initTB, boolean initTI) {
        label = initLabel;
        font = initFont;
        size = initSize;
        toggleBold = initTB;
        toggleItalics = initTI;
        oldFont = initLabel.getFont();
    }
    
    @Override
    public void doTransaction() {
        Font f = Font.font(font, size);
        label.setFont(f);
        if(toggleBold)
            label.toggleBold();
        if(toggleItalics)
            label.toggleItalics();
    }

    @Override
    public void undoTransaction() {
        label.setFont(oldFont);
        if(toggleBold)
            label.toggleBold();
        if(toggleItalics)
            label.toggleItalics();
    }
    
    @Override
    public String toString() {
        return "Change Font";
    }
}
