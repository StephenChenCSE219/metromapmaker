package m3.tps;

import jtps.jTPS_Transaction;
import m3.data.MetroLine;
import m3.data.m3Data;

public class AddLine_Transaction implements jTPS_Transaction{

    m3Data data;
    MetroLine line;
    
    public AddLine_Transaction(MetroLine initLine, m3Data initData) {
        data = initData;
        line = initLine;
    }
    
    @Override
    public void doTransaction() {
        data.getElements().add(line.getBegin());
        data.getElements().add(line.getConn());
        data.getElements().add(line.getEnd());
        data.getElements().add(line.getBegin().label);
        data.getElements().add(line.getEnd().label);
        data.selectLine(line);
        data.moveStationsToFront();
    }

    @Override
    public void undoTransaction() {
        data.getElements().remove(line.getBegin());
        data.getElements().remove(line.getConn());
        data.getElements().remove(line.getEnd());
        data.getElements().remove(line.getBegin().label);
        data.getElements().remove(line.getEnd().label);
        data.selectLine(0);
        data.moveStationsToFront();
    }
    
    @Override
    public String toString() {
        return "Add Line";
    }
}
