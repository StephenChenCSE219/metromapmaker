package m3.tps;

import jtps.jTPS_Transaction;
import m3.data.MetroStation;

public class MoveLabel_Transaction implements jTPS_Transaction{

    MetroStation station;
    
    public MoveLabel_Transaction(MetroStation initStation) {
        station = initStation;
    }
    
    @Override
    public void doTransaction() {
        station.moveLabel(1);
    }

    @Override
    public void undoTransaction() {
        station.moveLabel(-1);
    }
    
    @Override
    public String toString() {
        return "Move Station Label";
    }
}
