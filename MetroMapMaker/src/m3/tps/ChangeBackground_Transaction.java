package m3.tps;

import javafx.scene.layout.Background;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import m3.data.MetroImage;
import m3.data.m3Data;
import m3.gui.m3Workspace;

public class ChangeBackground_Transaction implements jTPS_Transaction{

    Background oldBackground;
    Background newBackground;
    String path;
    Pane pane;
    boolean isImage;
    
    public ChangeBackground_Transaction(Pane p, Background bg) {
        pane = p;
        oldBackground = p.getBackground();
        newBackground = bg;
        isImage = false;
    }
    
    public ChangeBackground_Transaction(Pane p, String ipath){
        pane = p;
        path = ipath;
        isImage = true;
    }
    
    @Override
    public void doTransaction() {
        if(isImage){
            pane.getChildren().add(new MetroImage(path));
        } else {
            pane.setBackground(newBackground);
        }
    }

    @Override
    public void undoTransaction() {
        if(isImage){
            pane.getChildren().clear();
        } else {
            pane.setBackground(oldBackground);
        }
    }
    
    @Override
    public String toString() {
        return "";
    }
}
