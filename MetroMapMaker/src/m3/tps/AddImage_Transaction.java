package m3.tps;

import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;
import m3.data.MetroImage;
import m3.data.m3Data;

public class AddImage_Transaction implements jTPS_Transaction{

    m3Data data;
    MetroImage image;
    
    public AddImage_Transaction(MetroImage initImage, m3Data initData) {
        data = initData;
        image = initImage;
    }
    
    @Override
    public void doTransaction() {
        data.getElements().add(image);
        data.setSelectedElement(image);
    }

    @Override
    public void undoTransaction() {
        data.getElements().remove(image);
        data.setSelectedElement(null);
    }
    
    @Override
    public String toString() {
        return "Create Image";
    }
}
