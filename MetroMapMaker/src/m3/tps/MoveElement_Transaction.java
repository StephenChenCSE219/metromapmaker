package m3.tps;

import jtps.jTPS_Transaction;
import m3.data.MetroElement;

public class MoveElement_Transaction implements jTPS_Transaction{

    private MetroElement element;
    private double oldX;
    private double oldY;
    private double newX;
    private double newY;
    
    public MoveElement_Transaction(MetroElement initShape, double startPosX, double startPosY, double endPosX, double endPosY) {
        element = initShape;
        oldX = startPosX;
        oldY = startPosY;
        newX = endPosX;
        newY = endPosY;
    }
    
    @Override
    public void doTransaction() {
        element.setLocation((int)newX, (int)newY);
    }

    @Override
    public void undoTransaction() {
        element.setLocation((int)oldX, (int)oldY);
    }
    
    @Override
    public String toString() {
        return "Move Element";
    }
}
