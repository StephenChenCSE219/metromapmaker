package m3.tps;

import jtps.jTPS_Transaction;
import m3.data.MetroStation;

public class RotateLabel_Transaction implements jTPS_Transaction{

    MetroStation station;
    
    public RotateLabel_Transaction(MetroStation initStation) {
        station = initStation;
    }
    
    @Override
    public void doTransaction() {
        station.rotateLabel(1);
    }

    @Override
    public void undoTransaction() {
        station.rotateLabel(-1);
    }
    
    @Override
    public String toString() {
        return "Rotate Station Label";
    }
}
