package m3.tps;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;
import m3.data.MetroConnection;
import m3.data.m3Data;

public class AddStationToLine_Transaction implements jTPS_Transaction{

    ObservableList<Node> old;
    ObservableList<Node> newL;
    m3Data data;
    
    public AddStationToLine_Transaction(ObservableList<Node> initOld, ObservableList<Node> initNew, m3Data initData) {
        old = FXCollections.observableArrayList();
        newL = FXCollections.observableArrayList();
        
        for (Node node : initOld)
            old.add(node);
        for (Node node : initNew)
            newL.add(node);
        
        data = initData;
    }
    
    @Override
    public void doTransaction() {
        data.getElements().clear();
        for (Node node : newL){
            if(node instanceof MetroConnection)
                ((Shape)node).setOpacity(255);
            data.getElements().add(node);
        }
    }

    @Override
    public void undoTransaction() {
        data.getElements().clear();
        for (Node node : old){
            if(node instanceof MetroConnection)
                ((Shape)node).setOpacity(255);
            data.getElements().add(node);
        }
    }
    
    @Override
    public String toString() {
        return "Add Station to Line";
    }
}
