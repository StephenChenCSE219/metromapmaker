package m3.tps;

import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;
import m3.data.m3Data;

public class RemoveElement_Transaction implements jTPS_Transaction{

    m3Data data;
    Shape element;
    
    public RemoveElement_Transaction(Shape selectedElement, m3Data aThis) {
        data = aThis;
        element = selectedElement;
    }
    
    @Override
    public void doTransaction() {
        data.getElements().remove(element);
    }

    @Override
    public void undoTransaction() {
        data.getElements().add(element);
    }
    
    @Override
    public String toString() {
        return "Remove Element";
    }
}
