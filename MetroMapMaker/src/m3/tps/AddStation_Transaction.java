package m3.tps;

import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;
import m3.data.MetroStation;
import m3.data.m3Data;

public class AddStation_Transaction implements jTPS_Transaction{
    
    m3Data data;
   
    private MetroStation station;
    
    public AddStation_Transaction(MetroStation initStation, m3Data initData) {
        station = initStation;
        data = initData;
    }
    
    @Override
    public void doTransaction() {
        data.getElements().add(station);
        data.getElements().add(station.label);
        data.setSelectedElement(station);
    }

    @Override
    public void undoTransaction() {
        data.getElements().remove(station);
        data.getElements().remove(station.label);
        data.setSelectedElement(null);
    }
    
    @Override
    public String toString() {
        return "Create Station";
    }
}
