package m3.tps;

import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

public class ChangeColor_Transaction implements jTPS_Transaction{

    Shape shape;
    Color oldColor;
    Color newColor;
    
    public ChangeColor_Transaction(Shape initShape, Color initColor) {
        shape = initShape;
        oldColor = (Color)initShape.getFill();
        newColor = initColor;
    }
    
    @Override
    public void doTransaction() {
        shape.setFill(newColor);
    }

    @Override
    public void undoTransaction() {
        shape.setFill(oldColor);
    }
    
    @Override
    public String toString() {
        return "Change Element Color";
    }
}
