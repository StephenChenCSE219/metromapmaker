package m3.tps;

import javafx.scene.control.Slider;
import jtps.jTPS_Transaction;
import m3.data.MetroStation;

public class ChangeStationRadius_Transaction implements jTPS_Transaction{

    private Slider slider;
    double oldPos;
    double newPos;
    MetroStation line;
    
    public ChangeStationRadius_Transaction(MetroStation initLine, Slider initSlider, double oPos, double nPos) {
        slider = initSlider;
        line = initLine;
        oldPos = oPos;
        newPos = nPos;
    }
    
    @Override
    public void doTransaction() {
        slider.setValue(newPos);
        line.setRadius(newPos);
    }

    @Override
    public void undoTransaction() {
        slider.setValue(oldPos);
        line.setRadius(oldPos);
    }
    
    @Override
    public String toString() {
        return "Line Thickness";
    }
}
