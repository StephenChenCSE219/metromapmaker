package m3;

import java.util.Locale;
import m3.data.m3Data;
import m3.file.m3Files;
import m3.gui.m3Workspace;
import djf.AppTemplate;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import static m3.m3LanguageProperty.APP_ICON;
import properties_manager.PropertiesManager;

/**
 * This class serves as the application class for our goLogoLoApp program. 
 * Note that much of its behavior is inherited from AppTemplate, as defined in
 * the Desktop Java Framework. This app starts by loading all the app-specific
 * messages like icon files and tooltips and other settings, then the full 
 * User Interface is loaded using those settings. Note that this is a 
 * JavaFX application.
 * 
 * @author Stephen X Chen
 * @version 1.0
 */
public class MetroMapMakerApp extends AppTemplate {
     /**
     * This hook method must initialize all three components in the
     * proper order ensuring proper dependencies are respected, meaning
     * all proper objects are already constructed when they are needed
     * for use, since some may need others for initialization.
     */
    @Override
    public void buildAppComponentsHook() {
        // CONSTRUCT ALL THREE COMPONENTS. NOTE THAT FOR THIS APP
        // THE WORKSPACE NEEDS THE DATA COMPONENT TO EXIST ALREADY
        // WHEN IT IS CONSTRUCTED, AND THE DATA COMPONENT NEEDS THE
        // FILE COMPONENT SO WE MUST BE CAREFUL OF THE ORDER
        fileComponent = new m3Files();
        dataComponent = new m3Data(this);
        try {
            workspaceComponent = new m3Workspace(this);
        } catch (IOException ex) {}
        showWelcomeDialog();
    }
    
    /**
     * This is where program execution begins. Since this is a JavaFX app it
     * will simply call launch, which gets JavaFX rolling, resulting in sending
     * the properly initialized Stage (i.e. window) to the start method inherited
     * from AppTemplate, defined in the Desktop Java Framework.
     */
    public static void main(String[] args) {
	Locale.setDefault(Locale.US);
	launch(args);
    }
    
        
    @SuppressWarnings("unchecked")
    private void showWelcomeDialog(){
        
        Stage stage = new Stage();
        stage.setResizable(false);
        
        stage.setTitle("Metro Map Maker - Welcome Dialog");
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        BorderPane pane = new BorderPane();
        pane.setTop(new ImageView(new Image("file:./images/" + props.getProperty(APP_ICON.toString()))));
        
        VBox newMapPane = new VBox();
        
        VBox recentsPane = new VBox(20);
        recentsPane.setAlignment(Pos.CENTER);
        Label recents = new Label("Recent Work:");
        recents.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        
        Text work1 = new Text("--");
        work1.setFont(Font.font(16));
        Text work2 = new Text("--");
        work2.setFont(Font.font(16));
        Text work3 = new Text("--");
        work3.setFont(Font.font(16));
        Text work4 = new Text("--");
        work4.setFont(Font.font(16));
        Text work5 = new Text("--");
        work5.setFont(Font.font(16));
        
        ArrayList<Text> workArray = new ArrayList<>();
        workArray.add(work1);
        workArray.add(work2);
        workArray.add(work3);
        workArray.add(work4);
        workArray.add(work5);

        ArrayList<File> files = new ArrayList<>();
        File work = new File("./work/");
        File[] fileList = work.listFiles();
        
        Arrays.sort(fileList, new Comparator(){
            public int compare(Object o1, Object o2) {
                return compare( (File)o1, (File)o2);
            }
            private int compare( File f1, File f2){
                long result = f2.lastModified() - f1.lastModified();
                if( result > 0 ){
                    return 1;
                } else if( result < 0 ){
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        
        for (File f : fileList){
            files.add(f);
            if (files.size() >= (Math.min(5, fileList.length)))
                break;
        }
        
        for (int i = 0; i < files.size(); i++){
            workArray.get(i).setText(files.get(i).getName());
            workArray.get(i).underlineProperty().set(true);
            workArray.get(i).setFill(Color.BLUE);
        }
        
        recentsPane.getChildren().add(recents);
        recentsPane.getChildren().add(work1);
        recentsPane.getChildren().add(work2);
        recentsPane.getChildren().add(work3);
        recentsPane.getChildren().add(work4);
        recentsPane.getChildren().add(work5);
        
        pane.setCenter(recentsPane);
        
        Insets inset = new Insets(0, 70, 0, 0);
        pane.setMargin(newMapPane, inset);
        
        newMapPane.setAlignment(Pos.CENTER_LEFT);
        
        Button createNewMap = new Button();
        createNewMap.setText("Create New Metro Map");
        createNewMap.setFont(Font.font(18));

        newMapPane.getChildren().add(createNewMap);
        pane.setRight(newMapPane);
        
        if (!work1.getText().equals("--")) {
            work1.setOnMouseClicked(e->{
            loadRecent(work1.getText());
            stage.hide();
	});}
        if (!work2.getText().equals("--")) {
            work2.setOnMouseClicked(e->{
            loadRecent(work2.getText());
            stage.hide();
	});}
        if (!work3.getText().equals("--")) {
            work3.setOnMouseClicked(e->{
            loadRecent(work3.getText());
            stage.hide();
	});}
        if (!work4.getText().equals("--")) {
            work4.setOnMouseClicked(e->{
            loadRecent(work4.getText());
            stage.hide();
	});}
        if (!work5.getText().equals("--")) {
            work5.setOnMouseClicked(e->{
            loadRecent(work5.getText());
            stage.hide();
	});}

        createNewMap.setOnAction(e->{
	    this.getGUI().getFileController().handleNewRequest();
            stage.hide();
	});
        
        stage.setScene(new Scene(pane, 660, 500));
        stage.showAndWait();
    }
    
    private void loadRecent(String path){
        try {
            // LOAD THE FILE INTO THE DATA
            getFileComponent().loadData(getDataComponent(), "./work/" + path);

            // MAKE SURE THE WORKSPACE IS ACTIVATED
            getWorkspaceComponent().activateWorkspace(getGUI().getAppPane());
            getGUI().updateToolbarControls(true);
        } catch (Exception e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
        }
    }
}