package m3.file;

import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import m3.data.MetroConnection;
import m3.data.MetroElement;
import m3.data.MetroImage;
import m3.data.MetroLabel;
import m3.data.MetroLine;
import m3.data.MetroStation;
import m3.data.m3Data;

public class m3Files implements AppFileComponent{

    static final String JSON_BG_COLOR = "background_color";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";
    static final String JSON_ELEMENTS = "shapes";
    static final String JSON_ELEMENT = "shape";
    static final String JSON_TYPE = "type";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_ENDX = "endx";
    static final String JSON_ENDY = "endy";
    static final String JSON_THICKNESS = "thickness";
    static final String JSON_POSITION = "position";
    static final String JSON_IMAGE = "image";
    
    static final String JSON_TEXT = "string";
    static final String JSON_PATH = "path";
    static final String JSON_FONT = "font";
    static final String JSON_BOLD = "bold";
    static final String JSON_ITALICS = "italics";
    static final String JSON_FONTFAMILY = "font";
    static final String JSON_FONTSIZE = "size";
    static final String JSON_ROTATION = "rotation";
    
    static final String DEFAULT_DOCTYPE_DECLARATION = "<!doctype html>\n";
    static final String DEFAULT_ATTRIBUTE_VALUE = "";
    
    //BENCHMARK 2 JSON
    static final String JSON_NAME = "name";
    static final String JSON_LINES = "lines";
    static final String JSON_CIRCULAR = "circular";
    static final String JSON_COLOR = "color";
    static final String JSON_STATION_NAMES = "station_names";
    static final String JSON_STATIONS = "stations";
    static final String JSON_LABELCOLOR = "labelcolor";
    static final String JSON_DECOR = "decor";
    
    static final String JSON_ID = "id";
    
    /**
     * Save the current map
     * @param data data to save
     * @param filePath path to save data at
     * @throws java.io.IOException
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
	m3Data dataManager = (m3Data)data;
	
	// FIRST THE BACKGROUND COLOR
	String mapName = dataManager.getMapName();
        JsonObject bgColorJson = makeJsonColorObject((Color)dataManager.getBackground().getBackground().getFills().get(0).getFill());
        String bgImage = "";
        if(!dataManager.getBackground().getChildren().isEmpty())
            bgImage = ((MetroImage)(dataManager.getBackground().getChildren().get(0))).getPath();
        
	// NOW BUILD THE JSON LINES TO SAVE
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        ArrayList<MetroLine> lines = dataManager.getLines();
        
        for (MetroLine line : lines){
            
            String name = line.toString();
            String id = line.id;
            JsonObject lineColorJson = makeJsonColorObject((Color)line.getConn().getStroke());
            JsonArrayBuilder innerArrayBuilder = Json.createArrayBuilder();
            int startX = (int) line.getBegin().getX();
            int startY = (int) line.getBegin().getY();
            int endX = (int) line.getEnd().getX();
            int endY = (int) line.getEnd().getY();
            for(int i = 1; i < line.getStations().size() - 1; i++)
                innerArrayBuilder.add(line.getStations().get(i).getID());
            double lineThickness = line.getConn().getStrokeWidth();
            JsonArray stationsArray = innerArrayBuilder.build();
            
            JsonObject lineJson = Json.createObjectBuilder()
                    .add(JSON_NAME, name)
                    .add(JSON_ID, id)
                    .add(JSON_COLOR, lineColorJson)
                    .add(JSON_X, startX)
                    .add(JSON_Y, startY)
                    .add(JSON_ENDX, endX)
                    .add(JSON_ENDY, endY)
                    .add(JSON_THICKNESS, lineThickness)
                    .add(JSON_STATION_NAMES, stationsArray)
                    .build();
            
            arrayBuilder.add(lineJson);
        }
        JsonArray linesArray = arrayBuilder.build();
        
        JsonArrayBuilder arrayBuilder2 = Json.createArrayBuilder();
        ArrayList<MetroStation> stations = dataManager.getStations();
        
        for (MetroStation station : stations){
            String name = station.toString();
            String id = station.getID();
            double xPos = station.getCenterX();
            double yPos = station.getCenterY();
            Font stationFont = station.label.getFont();
            boolean stationBold = station.label.isBold;
            boolean stationItalics = station.label.isItalics;
            JsonObject stationFontJson = makeJsonFontObject(stationFont, stationBold, stationItalics);
            JsonObject stationColorJson = makeJsonColorObject((Color)station.getFill());
            int labelPos = station.labelPos;
            JsonObject labelColorJson = makeJsonColorObject((Color)station.label.getFill());
            double rot = station.getRotate();
            double radius = station.getRadius();
            JsonObject stationJson = Json.createObjectBuilder()
                    .add(JSON_NAME, name)
                    .add(JSON_ID, id)
                    .add(JSON_COLOR, stationColorJson)
                    .add(JSON_X, xPos)
                    .add(JSON_Y, yPos)
                    .add(JSON_FONT, stationFontJson)
                    .add(JSON_POSITION, labelPos)
                    .add(JSON_LABELCOLOR, labelColorJson)
                    .add(JSON_ROTATION, rot)
                    .add(JSON_THICKNESS, radius)
                    .build();
            arrayBuilder2.add(stationJson);
        }
        JsonArray stationsArray = arrayBuilder2.build();
        
        JsonArrayBuilder arrayBuilder3 = Json.createArrayBuilder();
        ArrayList<MetroElement> decor = new ArrayList<>();
        for(Node n : dataManager.getElements()){
            if (n instanceof MetroImage)
                decor.add((MetroImage)n);
            if (n instanceof MetroLabel && !((MetroLabel)n).hasParent)
                decor.add((MetroLabel)n);
        }
        
        for (MetroElement element : decor){
            double xPos = element.getX();
            double yPos = element.getY();
            String path = "";
            
            Font labelFont = Font.font("Arial", 4);
            boolean labelBold = false;
            boolean labelItalics = false;
            JsonObject labelColorJson = makeJsonColorObject(Color.TRANSPARENT);
            if (element instanceof MetroLabel) {
                labelFont = ((MetroLabel)element).getFont();
                labelBold = ((MetroLabel)element).isBold;
                labelItalics = ((MetroLabel)element).isItalics;
                labelColorJson = makeJsonColorObject((Color)((MetroLabel)element).getFill());
                path = ((MetroLabel)element).getText();
            }
            
            if(element instanceof MetroImage)
                path = ((MetroImage)element).getPath();
            
            JsonObject labelFontJson = makeJsonFontObject(labelFont, labelBold, labelItalics);
            
            JsonObject decorJson = Json.createObjectBuilder()
                    .add(JSON_X, xPos)
                    .add(JSON_Y, yPos)
                    .add(JSON_COLOR, labelColorJson)
                    .add(JSON_FONT, labelFontJson)
                    .add(JSON_PATH, path)
                    .build();
            arrayBuilder3.add(decorJson);
        }
        JsonArray decorArray = arrayBuilder3.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_NAME, mapName)
                .add(JSON_BG_COLOR, bgColorJson)
                .add(JSON_IMAGE, bgImage)
		.add(JSON_LINES, linesArray)
                .add(JSON_STATIONS, stationsArray)
                .add(JSON_DECOR, decorArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath + ".json");
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath + ".json");
	pw.write(prettyPrinted);
	pw.close();
    }

    /**
     * Load a previously saved map
     * @param data data to load into
     * @param filePath path of saved map
     * @throws java.io.IOException
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        //System.out.println(filePath);

        // CLEAR THE OLD DATA OUT
	m3Data dataManager = (m3Data)data;
	dataManager.resetData();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
        String mapName = getDataAsString(json, JSON_NAME);
        dataManager.mapName = mapName;
        
	// LOAD THE BACKGROUND COLOR
	Color bgColor = loadColor(json, JSON_BG_COLOR);
	dataManager.getBackground().setBackground(new Background(new BackgroundFill(bgColor, null, null)));
	
        String bgImg = getDataAsString(json, JSON_IMAGE);
        if(!bgImg.equals(""))
            dataManager.getBackground().getChildren().add(new MetroImage(bgImg));
        
	// AND NOW LOAD ALL THE STATIONS
	JsonArray jsonStationArray = json.getJsonArray(JSON_STATIONS);
	for (int i = 0; i < jsonStationArray.size(); i++) {
	    JsonObject jsonStation = jsonStationArray.getJsonObject(i);
            MetroStation station = loadStation(jsonStation);
	    dataManager.addStation(station);
	}
        
	JsonArray jsonLineArray = json.getJsonArray(JSON_LINES);
        for (int i = 0; i < jsonLineArray.size(); i++) {
	    JsonObject jsonLine = jsonLineArray.getJsonObject(i);
            MetroLine line = loadLine(jsonLine, dataManager);
	    dataManager.addLine(line);
	}
        
        dataManager.addAllToComboBox();
        
	JsonArray jsonDecorArray = json.getJsonArray(JSON_DECOR);
        for (int i = 0; i < jsonDecorArray.size(); i++) {
	    JsonObject jsonDecor = jsonDecorArray.getJsonObject(i);
            MetroElement element;
            Font check = loadFont(jsonDecorArray.getJsonObject(i), JSON_FONT);
            if(check.getSize() == 4) //object is metroimage
                element = loadImage(jsonDecor);
            else
                element = loadLabel(jsonDecor);
	    dataManager.addElement((Shape)element);
	}        

        dataManager.moveStationsToFront();
//TODO: NOW JUST LOAD THE LABELS, IMAGES, ETC
        //THEN MOVE THE STATIONS AND LABELS TO THE TOP!
    }

    /**
     * Exports the data into a JSON and PNG file
     * @param data data to export
     * @param filePath path to folder to save data to
     * @throws java.io.IOException
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        
        // GET THE DATA
	m3Data dataManager = (m3Data)data;
	
        
	// FIRST THE BACKGROUND COLOR
	String mapName = dataManager.getMapName();

        //WRITE THE IMAGE!
        File imageFile = new File("work/"+ mapName + "/" + mapName + " Metro.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(dataManager.getMapImage(), null), "png", imageFile);
        } catch (IOException e) {}
        
	// NOW BUILD THE JSON LINES TO SAVE
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        ArrayList<MetroLine> lines = dataManager.getLines();
        
        for (MetroLine line : lines){
            
            String name = line.toString();
            boolean circular = false;
            JsonObject lineColorJson = makeJsonColorObject((Color)line.getConn().getStroke());
            JsonArrayBuilder innerArrayBuilder = Json.createArrayBuilder();
            for(int i = 1; i < line.getStations().size() - 1; i++)
                innerArrayBuilder.add(line.getStations().get(i).toString());
            JsonArray stationsArray = innerArrayBuilder.build();
            
            JsonObject lineJson = Json.createObjectBuilder()
                    .add(JSON_NAME, name)
                    .add(JSON_CIRCULAR, circular)
                    .add(JSON_COLOR, lineColorJson)
                    .add(JSON_STATION_NAMES, stationsArray)
                    .build();
            
            arrayBuilder.add(lineJson);
        }
        JsonArray linesArray = arrayBuilder.build();
        
        JsonArrayBuilder arrayBuilder2 = Json.createArrayBuilder();
        ArrayList<MetroStation> stations = dataManager.getStations();
        
        for (MetroStation station : stations){
            String name = station.toString();
            double xPos = station.getCenterX();
            double yPos = station.getCenterY();
            JsonObject stationJson = Json.createObjectBuilder()
                    .add(JSON_NAME, name)
                    .add(JSON_X, xPos)
                    .add(JSON_Y, yPos)
                    .build();
            arrayBuilder2.add(stationJson);
        }
        JsonArray stationsArray = arrayBuilder2.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_NAME, mapName)
		.add(JSON_LINES, linesArray)
                .add(JSON_STATIONS, stationsArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream("work/"+ mapName + "/" + mapName + " Metro.json");
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter("work/"+ mapName + "/" + mapName + " Metro.json");
	pw.write(prettyPrinted);
	pw.close();
        
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Export");
        alert.setHeaderText("Successfully exported.");
        alert.showAndWait();
    }
    
    /**
     * Imports data from JSON file
     * @param data data to load into
     * @param filePath path of file to import from
     * @throws java.io.IOException
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
    }
    
    private JsonObject makeJsonColorObject(Color color){
        JsonObject colorJson = Json.createObjectBuilder()
		.add(JSON_RED, color.getRed())
		.add(JSON_GREEN, color.getGreen())
		.add(JSON_BLUE, color.getBlue())
		.add(JSON_ALPHA, color.getOpacity()).build();
	return colorJson;
    }
    
    private JsonObject makeJsonFontObject(Font font, boolean isBold, boolean isItalics){
        JsonObject fontJson = Json.createObjectBuilder()
		.add(JSON_FONTFAMILY, font.getFamily())
		.add(JSON_FONTSIZE, font.getSize())
		.add(JSON_BOLD, isBold)
                .add(JSON_ITALICS, isItalics)
                .build();
	return fontJson;
    }
    
    private String getDataAsString (JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonString string = (JsonString)value;
        return string.getString();
    }
    
    private boolean getDataAsBoolean(JsonObject json, String dataName){
        return json.getBoolean(dataName);
    }
    
    private MetroStation loadStation(JsonObject JsonElement){
        MetroStation station = new MetroStation();
        double posX = getDataAsDouble(JsonElement, JSON_X);
        double posY = getDataAsDouble(JsonElement, JSON_Y);
        String name = getDataAsString(JsonElement, JSON_NAME);
        String id = getDataAsString(JsonElement, JSON_ID);
        Color color = loadColor(JsonElement, JSON_COLOR);
        Font font = loadFont(JsonElement, JSON_FONT);
        int labelPos = getDataAsInt(JsonElement, JSON_POSITION);
        Color lcolor = loadColor(JsonElement, JSON_LABELCOLOR);
        double rot = getDataAsDouble(JsonElement, JSON_ROTATION);
        double rad = getDataAsDouble(JsonElement, JSON_THICKNESS);
        
        station.setName(name);
        station.setID(id);
        station.setFill(color);
        station.setStroke(Color.BLACK);
        station.setStrokeWidth(3);
        station.setCenterX(posX);
        station.setCenterY(posY);
        station.label.setFont(font);
        station.label.setFill(lcolor);
        station.label.setRotate(rot);
        station.setRadius(rad);
        station.setLabelPos(labelPos);
        
        return station;
    }
    
    private double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    private int getDataAsInt(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().intValue();	
    }
    
    private Color loadColor(JsonObject json, String colorToGet){
	JsonObject jsonColor = json.getJsonObject(colorToGet);
	double red = getDataAsDouble(jsonColor, JSON_RED);
	double green = getDataAsDouble(jsonColor, JSON_GREEN);
	double blue = getDataAsDouble(jsonColor, JSON_BLUE);
	double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
	Color loadedColor = new Color(red, green, blue, alpha);
	return loadedColor;
    }
    
    private Font loadFont(JsonObject json, String fontToGet){
	JsonObject jsonFont = json.getJsonObject(fontToGet);
	String family = getDataAsString(jsonFont, JSON_FONTFAMILY);
        int size = getDataAsInt(jsonFont, JSON_FONTSIZE);
        boolean bold = getDataAsBoolean(jsonFont, JSON_BOLD);
        boolean italics = getDataAsBoolean(jsonFont, JSON_ITALICS);
        FontWeight w = FontWeight.NORMAL;
        FontPosture p = FontPosture.REGULAR;
        if (bold)
            w = FontWeight.BOLD;
        if (italics)
            p = FontPosture.ITALIC;
        Font loadedFont = Font.font(family, w, p, size);
	return loadedFont;
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    private MetroLine loadLine(JsonObject jsonLine, m3Data data) {
        MetroLine line = new MetroLine();
        String name = getDataAsString(jsonLine, JSON_NAME);
        String id = getDataAsString(jsonLine, JSON_ID);
        Color color = loadColor(jsonLine, JSON_COLOR);
        JsonArray jsonStationArray = jsonLine.getJsonArray(JSON_STATION_NAMES);
        int startX = getDataAsInt(jsonLine, JSON_X);
        int startY = getDataAsInt(jsonLine, JSON_Y);
        int endX = getDataAsInt(jsonLine, JSON_ENDX);
        int endY = getDataAsInt(jsonLine, JSON_ENDY);
        int thickness = getDataAsInt(jsonLine, JSON_THICKNESS);
        
        line.getBegin().setLocation(startX, startY);
        line.getEnd().setLocation(endX, endY);
        
        line.setName(name);
        line.setID(id);
	
        line.getStations().remove(line.getEnd());
        
        for (int i = 0; i < jsonStationArray.size(); i++) {
            String stationID = jsonStationArray.get(i).toString().replace("\"", "");
            for(MetroStation m : data.getStations()){
                if(m.getID().equals(stationID)){
                    line.getStations().add(m);
                }
            }
	}
        
        line.getStations().add(line.getEnd());
        
        MetroConnection conn = new MetroConnection(line.getBegin(), line.getStations().get(1));
        line.setConn(conn);
        
        for (int i = 1; i < line.getStations().size()-1; i++){
            MetroConnection cPrev = new MetroConnection(line.getStations().get(i-1), line.getStations().get(i));
            MetroConnection cNext = new MetroConnection(line.getStations().get(i), line.getStations().get(i+1));
            line.getStations().get(i).connections.add(cPrev);
            line.getStations().get(i).connections.add(cNext);
            data.getElements().add(cPrev);
            data.getElements().add(cNext);
        }
        
        //MetroConnection conn2 = new MetroConnection(line.getStations().get(line.getStations().size()-2), line.getEnd());
        
        line.setColor(color);
        line.setThickness(thickness);
        
        //line.setStations(stations);
        
        return line;
    }
    
    private MetroLabel loadLabel(JsonObject JsonElement){
        int xpos = getDataAsInt(JsonElement, JSON_X);
        int ypos = getDataAsInt(JsonElement, JSON_Y);
        Font font = loadFont(JsonElement, JSON_FONT);
        Color fill = loadColor(JsonElement, JSON_COLOR);
        String content = getDataAsString(JsonElement, JSON_PATH);
        MetroLabel label = new MetroLabel(content);
        label.setLocation(xpos, ypos);
        label.setFill(fill);
        label.setFont(font);
        return label;
    }
    
    private MetroImage loadImage(JsonObject JsonElement){
        int xpos = getDataAsInt(JsonElement, JSON_X);
        int ypos = getDataAsInt(JsonElement, JSON_Y);
        String content = getDataAsString(JsonElement, JSON_PATH);
        MetroImage img = new MetroImage(content);
        return img;
    }
}
